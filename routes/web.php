<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\CheckAge;



Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user1/{id}/profile', 'UserController@profile')->name('profile');

Route::resource('/action', 'ActionController');
Route::get('/actionanydata', 'ActionController@anyData');
Route::resource('/category', 'CategoryController');
Route::get('/categoryanydata', 'CategoryController@anyData');
Route::resource('/crud', 'CRUDController');
Route::get('/crudanydata', 'CRUDController@anyData');
Route::resource('/group', 'GroupController');
Route::get('/groupanydata', 'GroupController@anyData');
Route::resource('/lang', 'LangController');
Route::get('/langanydata', 'LangController@anyData');
Route::resource('/menu', 'MenuController');
Route::get('/menuanydata', 'MenuController@anyData');
Route::patch('/menu/{id}/show', 'MenuController@showSts');
Route::patch('/menu/{id}/hide', 'MenuController@hideSts');
Route::resource('/menufront', 'MenuFrontController');
Route::get('/menufrontanydata', 'MenuFrontController@anyData');
Route::patch('/menufront/{id}/show', 'MenuFrontController@showSts');
Route::patch('/menufront/{id}/hide', 'MenuFrontController@hideSts');
Route::resource('/post', 'PostController');
Route::get('/postanydata', 'PostController@anyData');
Route::patch('/post/{id}/show', 'PostController@showSts');
Route::patch('/post/{id}/hide', 'PostController@hideSts');
Route::resource('/setting', 'SettingController');
Route::get('/settinganydata', 'SettingController@anyData');
Route::resource('/user', 'UserController');
Route::get('/useranydata', 'UserController@anyData');

// Route::get('foo/{id}/{name?}', function ($id, $name = null) {
// 	return 'Hello Word ' . $name;
// })->where(['id' => '[0-9]+', 'name', '[A-Za-z]+']);


// Route::get('user/{id}/profile', function ($id) {
//     return 'user/' . $id . '/profile ' . route('profile', ['id' => $id]);
// })->where('id', '[0-9]+')->name('profile');


// Route::get('redirect', function () {
//     return redirect()->route('profile', ['id' => 1]);
// });


// Route::get('checkAge',function () {

// })->middleware(checkAge::class);


// Route::get('/mweb', function () {
//     //
// })->middleware('web');
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="ModalLaravelAddLabel">View Action</h4>
  </div>
  <div class="modal-body">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-lg">Action Code</label>
      <div class="col-sm-9">
        {{$action->action_kode}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-lg">Action Label</label>
      <div class="col-sm-9">
        <?php
        if(@$action->label && $action->label != null){
          $_label = explode('|',$action->label);
          $_labelHtml = '';
          if(count($_label) > 0){
            $i = 0;
            foreach($_label as $r){
              $_labelHtml .= ($i==0)?$r:'<br>'.$r;
              $i++;
            };
          }
          echo $action->label = $_labelHtml;
        }
        ?>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Created</label>
      <div class="col-sm-9">
        {{$action->created_at}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Updated</label>
      <div class="col-sm-9">
        {{$action->updated_at}}
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>
<div class="modal-content">
  <form id="formModalAdd" method="post" action="{{action('ActionController@update', $id)}}">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="ModalLaravelAddLabel">Edit Action</h4>
  </div>
  <div class="modal-body">
    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
    <div class="form-group row">
      {{csrf_field()}}
      <input name="_method" type="hidden" value="PATCH">
      <label class="col-sm-3 col-form-label col-form-label-lg">Action Code</label>
      <div class="col-sm-9">
        <input type="text" class="form-control form-control-lg" placeholder="Action Code" name="action_kode" value="{{$action->action_kode}}">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-lg">Action Label</label>
      <div class="col-sm-9">
        <?php
        $_labelHtml = array();
        if(@$action->label && $action->label != null){
          $_label = explode('|',$action->label);
          if(count($_label) > 0){
            foreach($_label as $r){
              $_r = explode(',',$r);
              $_labelHtml[$_r[0]] = $_r[1];
            };
          }
        }
        ?>

        @foreach ($langs as $lang)
          <input type="hidden" name="action_lang[{{ $lang['lang_kode'] }}]" value="{{ $lang['lang_kode'] }}">
          <div class="input-group" style="margin-bottom: 5px;">
            <span class="input-group-addon">{{ $lang['lang_kode'] }}</span>
            <input type="text" class="form-control form-control-lg" placeholder="Action Label {{ $lang['lang_kode'] }}" name="action_label[{{ $lang['lang_kode'] }}]" value="<?=(@$_labelHtml[$lang['lang_kode']])?$_labelHtml[$lang['lang_kode']]:'';?>">
          </div>
        @endforeach
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button id="btnSubmit" type="button" class="btn btn-primary">Save changes</button>
  </div>
  </form>
</div>

<script>
  $('#btnSubmit').on('click', function () {
    var frm = $('#formModalAdd');
    $.ajax({
      type: frm.attr('method'),
      url: frm.attr('action'),
      data: frm.serialize(),
      dataType: "json",
      success: function (data) {
        if(data == true) {
          $('#ModalLaravelAdd').modal('hide');
          swal(
            'Success!',
            'Your data has been saved.',
            'success'
          );
          table.ajax.reload();
        }else{
          swal(
            'Failed!',
            'Your data not saved :)',
            'error'
          )
        }
      },
      error: function (data) {
        console.log(data);
      }
    });
  });
</script>
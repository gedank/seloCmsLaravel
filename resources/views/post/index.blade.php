@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Post
          <div class="btn-group pull-right">
            <a href="{{url('post/create')}}" class="btn btn-xs btn-success">add</a>
          </div>
          <span class="clearfix"></span>
        </div>
        <div class="panel-body">
          <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="data-table">
            <thead>
              <tr>
                <th rowspan="2">Category</th>
                <th rowspan="2">Slug</th>
                <th rowspan="2">Title</th>
                <th rowspan="2">Created</th>
                <th rowspan="2">Updated</th>
                <th colspan="4" width="15%">Action</th>
              </tr>
              <tr>
                <th>Status</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  var table;
  $(function() {
    table = $('#data-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! url('postanydata') !!}',
      columns: [
        { data: 'post_category_kode', name: 'post_category_kode' },
        // { 
        //   data: 'post_img', 
        //   orderable: false,
        //   mRender: function (post_img) {
        //     if(post_img != '' && post_img != null){
        //       var _file = post_img.replace('photos','photos/thumb')
        //       var _img = '<img src="<?php echo asset("storage")?>/'+_file+'">';
        //     }else{
        //       var _img = '<img src="<?php echo asset("storage/photos/thumb/default.png")?>">';
        //     }
        //     return _img;
        //   } 
        // },
        { 
          data: 'slug', 
          orderable: false,
          mRender: function (slug) {
            var _slug = slug.split('|');
            var _slugHtml = '';
            if(_slug.length > 0){
              var i = 0;
              _slug.forEach(function(list) {
                _slugHtml += (i==0)?list:'<br>'+list;
                i++;
              });
            }
            return _slugHtml;
          } 
        },
        { 
          data: 'post', 
          orderable: false,
          mRender: function (label) {
            var _label = label.split('|');
            var _labelHtml = '';
            if(_label.length > 0){
              var i = 0;
              _label.forEach(function(list) {
                _labelHtml += (i==0)?list:'<br>'+list;
                i++;
              });
            }
            return _labelHtml;
          } 
        },
        { data: 'created_at', name: 'created_at' },
        { data: 'updated_at', name: 'updated_at' },
        {
          data: null,
          orderable: false,
          mRender: function (data, type, row) {
            if(data.post_is_show=='N'){
              return '<form id="form-'+data.post_id+'" action="{{url('post')}}/'+data.post_id+'/show" method="post">'
                + '{{csrf_field()}}'
                + '<input name="_method" type="hidden" value="PATCH">'
                + '<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">'
                + '<input type="hidden" name="post_is_show" value="Y">'
                + '<button class="btn btn-xs btn-default" type="button" onclick="sts('+data.post_id+')">Hide</button>'
              + '</form>';
            }else{
              return '<form id="form-'+data.post_id+'" action="{{url('post')}}/'+data.post_id+'/hide" method="post">'
                + '{{csrf_field()}}'
                + '<input name="_method" type="hidden" value="PATCH">'
                + '<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">'
                + '<input type="hidden" name="post_is_show" value="N">'
                + '<button class="btn btn-xs btn-info" type="button" onclick="sts('+data.post_id+')">Show</button>'
              + '</form>';
            }
          }
        },
        {
          data: "post_id",
          orderable: false,
          mRender: function (post_id) {
            return '<a href="{{url('post')}}/'+post_id+'" class="btn btn-xs btn-primary">View</a>';
          }
        },
        {
          data: "post_id",
          orderable: false,
          mRender: function (post_id) {
            return '<a href="{{url('post')}}/'+post_id+'/edit" class="btn btn-xs btn-warning">Edit</a>';
          }
        },
        {
          data: "post_id",
          orderable: false,
          mRender: function (post_id) {
            return '<form id="form-'+post_id+'" action="{{url('post')}}/'+post_id+'" method="post">'
              + '{{csrf_field()}}'
              + '<input name="_method" type="hidden" value="DELETE">'
              + '<button class="btn btn-xs btn-danger" type="button" onclick="del('+post_id+')">Delete</button>'
            + '</form>';
          }
        }
      ]
    });
  });
</script>
@endsection
@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Edit Post
        </div>
        <form id="formModalAdd" method="post" action="{{ action('PostController@update', $id) }}" enctype="multipart/form-data">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PATCH">
        <?php
        $_titleHtml = array();
        $_postHtml = array();
        if(@$post->post && $post->post != null){
          $_post = explode('||',$post->post);
          if(count($_post) > 0){
            foreach($_post as $r){
              $_r = explode('|',$r);
              $_titleHtml[$_r[0]] = $_r[1];
              $_postHtml[$_r[0]] = $_r[2];
            };
          }
        }
        ?>
        <div class="panel-body">
          <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
          <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 15px;">
            <?php 
            $i = 0; 
            if(count($langs)>1){
              foreach($langs as $lang){
                $class = ($i==0)?'active':'';
                ?>
                <li role="presentation" class="<?=$class?>"><a href="#<?=$lang['lang_kode']?>" aria-controls="<?=$lang['lang_kode']?>" role="tab" data-toggle="tab"><?=$lang['lang_name']?></a></li>
                <?php
                $i++;
              }
            }
            ?>
          </ul>
          <div class="row">
            <div class="col-md-8">
              <div class="tab-content">
                <?php 
                $i = 0; 
                foreach($langs as $lang){
                  $class = ($i==0)?'active':'';
                  ?>
                  <div role="tabpanel" class="tab-pane <?=$class?>" id="<?=$lang['lang_kode']?>">
                    <input type="hidden" name="post_lang[<?=$lang['lang_kode']?>]" value="<?=$lang['lang_kode']?>">
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label col-form-label-lg">Title [<?=$lang['lang_kode']?>]</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control form-control-lg" placeholder="Title" name="post_title[<?=$lang['lang_kode']?>]" value="<?=(@$_titleHtml[$lang['lang_kode']])?$_titleHtml[$lang['lang_kode']]:'';?>">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label col-form-label-lg">Content [<?=$lang['lang_kode']?>]</label>
                      <div class="col-sm-10">
                        <textarea class="form-control form-control-lg" placeholder="Content" name="post_post[<?=$lang['lang_kode']?>]"><?=(@$_postHtml[$lang['lang_kode']])?$_postHtml[$lang['lang_kode']]:'';?></textarea>
                      </div>
                    </div>
                  </div>
                  <?php
                  $i++;
                }
                ?>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label col-form-label-lg">Category</label>
                <div class="col-sm-8">
                  <select class="form-control form-control-lg" name="post_category_kode">
                    <option value="">-- pilih --</option>
                    @foreach ($categories as $category)
                      <option value="{{ $category->category_kode }}" <?=($category->category_kode==$post->post_category_kode)?'selected="selected"':'';?>>{{ $category->category_label }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label col-form-label-lg">Tag</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control form-control-lg" placeholder="Tags" name="post_keyword" value="<?=(@$post->post_keyword)?$post->post_keyword:'';?>">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label col-form-label-lg">Photos</label>
                <div class="col-sm-8">
                  <?php if(@$post->post_img && $post->post_img != null && $post->post_img != ''){ ?>
                    <img src="<?php echo asset("storage/".$post->post_img)?>" style="max-width: 220px;margin-bottom: 15px;">
                  <?php }?>
                  <input type="file" class="form-control form-control-lg" placeholder="Photos" name="photos[]" multiple>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="panel-footer text-right">
          <a href="{{url('post')}}" class="btn btn-default">Cancle</a>
          <button id="btnSubmit" type="button" class="btn btn-primary">Save</button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  $('#btnSubmit').on('click', function () {
    var frm = $('#formModalAdd');
    $.ajax({
      type: frm.attr('method'),
      url: frm.attr('action'),
      data: new FormData($('#formModalAdd')[0]),
      contentType: false,
      cache: false,
      processData:false,
      dataType: "json",
      success: function (data) {
        if(data == true) {
          swal({
            title: 'Success!',
            text: 'Your data has been saved.',
            type: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
          }).then(function () {
            window.location = "{{ url('post') }}";
          });
        }else{
          swal(
            'Failed!',
            'Your data not saved :)',
            'error'
          )
        }
      },
      error: function (data) {
        console.log(data);
      }
    });
  });
</script>
@endsection
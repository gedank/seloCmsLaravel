@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          View Post
        </div>
        <?php
        $_titleHtml = array();
        $_postHtml = array();
        if(@$post->post && $post->post != null){
          $_post = explode('||',$post->post);
          if(count($_post) > 0){
            foreach($_post as $r){
              $_r = explode('|',$r);
              $_titleHtml[$_r[0]] = $_r[1];
              $_postHtml[$_r[0]] = $_r[2];
            };
          }
        }
        $_slugHtml = array();
        if(@$post->slug && $post->slug != null){
          $_slug = explode('|',$post->slug);
          if(count($_slug) > 0){
            foreach($_slug as $r){
              $_r = explode(',',$r);
              $_slugHtml[$_r[0]] = $_r[1];
            };
          }
        }
        ?>
        <div class="panel-body">
          <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 15px;">
            <?php 
            $i = 0; 
            if(count($langs)>1){
              foreach($langs as $lang){
                $class = ($i==0)?'active':'';
                ?>
                <li role="presentation" class="<?=$class?>"><a href="#<?=$lang['lang_kode']?>" aria-controls="<?=$lang['lang_kode']?>" role="tab" data-toggle="tab"><?=$lang['lang_name']?></a></li>
                <?php
                $i++;
              }
            }
            ?>
          </ul>
          <div class="row">
            <div class="col-md-8">
              <div class="tab-content">
                <?php 
                $i = 0; 
                foreach($langs as $lang){
                  $class = ($i==0)?'active':'';
                  ?>
                  <div role="tabpanel" class="tab-pane <?=$class?>" id="<?=$lang['lang_kode']?>">
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label col-form-label-lg">Slug [<?=$lang['lang_kode']?>]</label>
                      <div class="col-sm-10">
                        <?=(@$_slugHtml[$lang['lang_kode']])?$_slugHtml[$lang['lang_kode']]:'';?>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label col-form-label-lg">Title [<?=$lang['lang_kode']?>]</label>
                      <div class="col-sm-10">
                        <?=(@$_titleHtml[$lang['lang_kode']])?$_titleHtml[$lang['lang_kode']]:'';?>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label col-form-label-lg">Content [<?=$lang['lang_kode']?>]</label>
                      <div class="col-sm-10">
                        <?=(@$_postHtml[$lang['lang_kode']])?$_postHtml[$lang['lang_kode']]:'';?>
                      </div>
                    </div>
                  </div>
                  <?php
                  $i++;
                }
                ?>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label col-form-label-lg">Category</label>
                <div class="col-sm-8">
                  <?=(@$post->post_category_kode)?$post->post_category_kode:'';?>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label col-form-label-lg">Tag</label>
                <div class="col-sm-8">
                  <?=(@$post->post_keyword)?$post->post_keyword:'';?>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label col-form-label-lg">Photos</label>
                <div class="col-sm-8">
                  <?php if(@$post->post_img && $post->post_img != null && $post->post_img != ''){ ?>
                    <img src="<?php echo asset("storage/".$post->post_img)?>" style="max-width: 220px;">
                  <?php }else{ ?>
                    <img src="<?php echo asset("storage/photos/default.png")?>" style="max-width: 220px;">
                  <?php }?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="panel-footer text-right">
          <a href="{{url('post')}}" class="btn btn-default">Back</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
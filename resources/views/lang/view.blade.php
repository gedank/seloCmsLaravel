<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="ModalLaravelAddLabel">View Lang</h4>
  </div>
  <div class="modal-body">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-lg">Lang Code</label>
      <div class="col-sm-9">
        {{$lang->lang_kode}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Lang Name</label>
      <div class="col-sm-9">
        {{$lang->lang_name}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Lang Icon</label>
      <div class="col-sm-9">
        {{$lang->lang_icon}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Created</label>
      <div class="col-sm-9">
        {{$lang->created_at}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Updated</label>
      <div class="col-sm-9">
        {{$lang->updated_at}}
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>
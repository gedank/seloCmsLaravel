<div class="modal-content">
  <form id="formModalAdd" method="post" action="{{action('SettingController@update', $id)}}">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="ModalLaravelAddLabel">Edit Attribute</h4>
  </div>
  <div class="modal-body">
    <div class="form-group row">
      {{csrf_field()}}
      <input name="_method" type="hidden" value="PATCH">
      <label class="col-sm-3 col-form-label col-form-label-lg">Name</label>
      <div class="col-sm-9">
        <input type="text" class="form-control form-control-lg" placeholder="Name" name="setting_name" value="{{$setting->setting_name}}">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-lg">Value</label>
      <div class="col-sm-9">
        <input type="text" class="form-control form-control-lg" placeholder="Value" name="setting_value" value="{{$setting->setting_value}}">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-lg">Description</label>
      <div class="col-sm-9">
        <textarea class="form-control form-control-lg" placeholder="Description" name="setting_desc">{{$setting->setting_desc}}</textarea>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button id="btnSubmit" type="button" class="btn btn-primary">Save changes</button>
  </div>
  </form>
</div>

<script>
  $('#btnSubmit').on('click', function () {
    var frm = $('#formModalAdd');
    $.ajax({
      type: frm.attr('method'),
      url: frm.attr('action'),
      data: frm.serialize(),
      dataType: "json",
      success: function (data) {
        if(data == true) {
          $('#ModalLaravelAdd').modal('hide');
          swal(
            'Success!',
            'Your data has been saved.',
            'success'
          );
          table.ajax.reload();
        }else{
          swal(
            'Failed!',
            'Your data not saved :)',
            'error'
          )
        }
      },
      error: function (data) {
        console.log(data);
      }
    });
  });
</script>
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="ModalLaravelAddLabel">View Menu</h4>
  </div>
  <div class="modal-body">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-lg">Parent</label>
      <div class="col-sm-9">
        {{$menufront->parent_name}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Menu Name</label>
      <div class="col-sm-9">
        <?php
        if(@$menufront->menu_name && $menufront->menu_name != null){
          $_menu = explode('|',$menufront->menu_name);
          $_menuHtml = '';
          if(count($_menu) > 0){
            $i = 0;
            foreach($_menu as $r){
              $_menuHtml .= ($i==0)?$r:'<br>'.$r;
              $i++;
            };
          }
          echo $menufront->menu_name = $_menuHtml;
        }
        ?>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Slug</label>
      <div class="col-sm-9">
		<?php
        if(@$menufront->slug && $menufront->slug != null){
          $_slug = explode('|',$menufront->slug);
          $_slugHtml = '';
          if(count($_slug) > 0){
            $i = 0;
            foreach($_slug as $r){
              $_slugHtml .= ($i==0)?$r:'<br>'.$r;
              $i++;
            };
          }
          echo $menufront->slug = $_slugHtml;
        }
        ?>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Order</label>
      <div class="col-sm-9">
        {{$menufront->menu_order}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Description</label>
      <div class="col-sm-9">
        {{$menufront->menu_desc}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Show</label>
      <div class="col-sm-9">
        {{$menufront->menu_is_show}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Icon</label>
      <div class="col-sm-9">
        {{$menufront->menu_icon_large}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Icon Small</label>
      <div class="col-sm-9">
        {{$menufront->menu_icon_small}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Created</label>
      <div class="col-sm-9">
        {{$menufront->created_at}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Updated</label>
      <div class="col-sm-9">
        {{$menufront->updated_at}}
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>
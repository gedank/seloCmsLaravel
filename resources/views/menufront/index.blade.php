@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Menu Front
          <div class="btn-group pull-right">
            <a href="#" class="btn btn-xs btn-success" onclick="formModalAdd('{{url('menufront/create')}}')">add</a>
          </div>
          <span class="clearfix"></span>
        </div>
        <div class="panel-body">
          <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="data-table">
            <thead>
              <tr>
                <th rowspan="2">Name</th>
                <th rowspan="2">Parent</th>
                <th rowspan="2">Slug</th>
                <th rowspan="2">Created</th>
                <th rowspan="2">Updated</th>
                <th colspan="4" width="15%">Action</th>
              </tr>
              <tr>
                <th>Status</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  var table;
  $(function() {
    table = $('#data-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! url('menufrontanydata') !!}',
      columns: [
        { data: 'name', orderable: false, name: 'name' },
        { data: 'parent_name', name: 'parent_name' },
        { 
          data: 'slug', 
          orderable: false,
          mRender: function (slug) {
            var _slug = slug.split('|');
            var _slugHtml = '';
            if(_slug.length > 0){
              var i = 0;
              _slug.forEach(function(list) {
                _slugHtml += (i==0)?list:'<br>'+list;
                i++;
              });
            }
            return _slugHtml;
          } 
        },
        { data: 'created_at', name: 'created_at' },
        { data: 'updated_at', name: 'updated_at' },
        {
          data: null,
          orderable: false,
          mRender: function (data, type, row) {
            if(data.menu_is_show=='N'){
              return '<form id="form-'+data.menu_id+'" action="{{url('menufront')}}/'+data.menu_id+'/show" method="post">'
                + '{{csrf_field()}}'
                + '<input name="_method" type="hidden" value="PATCH">'
                + '<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">'
                + '<input type="hidden" name="menu_is_show" value="Y">'
                + '<button class="btn btn-xs btn-default" type="button" onclick="sts('+data.menu_id+')">Hide</button>'
              + '</form>';
            }else{
              return '<form id="form-'+data.menu_id+'" action="{{url('menufront')}}/'+data.menu_id+'/hide" method="post">'
                + '{{csrf_field()}}'
                + '<input name="_method" type="hidden" value="PATCH">'
                + '<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">'
                + '<input type="hidden" name="menu_is_show" value="N">'
                + '<button class="btn btn-xs btn-info" type="button" onclick="sts('+data.menu_id+')">Show</button>'
              + '</form>';
            }
          }
        },
        {
          data: "menu_id",
          orderable: false,
          mRender: function (menu_id) {
            return '<a href="#" class="btn btn-xs btn-primary" onclick="formModalAdd(\'{{url('menufront')}}/'+menu_id+'\')">View</a>';
          }
        },
        {
          data: "menu_id",
          orderable: false,
          mRender: function (menu_id) {
            return '<a href="#" class="btn btn-xs btn-warning" onclick="formModalAdd(\'{{url('menufront')}}/'+menu_id+'/edit\')">Edit</a>';
          }
        },
        {
          data: "menu_id",
          orderable: false,
          mRender: function (menu_id) {
            return '<form id="form-'+menu_id+'" action="{{url('menufront')}}/'+menu_id+'" method="post">'
              + '{{csrf_field()}}'
              + '<input name="_method" type="hidden" value="DELETE">'
              + '<button class="btn btn-xs btn-danger" type="button" onclick="del('+menu_id+')">Delete</button>'
            + '</form>';
          }
        }
      ]
    });
  });
</script>
@endsection
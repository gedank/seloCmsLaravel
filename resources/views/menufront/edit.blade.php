<div class="modal-content">
  <form id="formModalAdd" method="post" action="{{action('MenuFrontController@update', $id)}}">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="ModalLaravelAddLabel">Edit Menu</h4>
  </div>
  <div class="modal-body">
    {{csrf_field()}}
    <div class="panel-body">
      <input name="_method" type="hidden" value="PATCH">
      <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
      <div class="form-group row">
        <label class="col-sm-3 col-form-label col-form-label-lg">Parent</label>
        <div class="col-sm-9">
          <select class="form-control form-control-lg" name="menu_parent_id">
            <option value="0">-- pilih --</option>
            @foreach ($parents as $parent)
              <option value="{{ $parent['id'] }}" @if ($menufront->menu_parent_id == $parent['id']) selected @endif>{{ $parent['name'] }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label col-form-label-lg">Menu Name</label>
        <div class="col-sm-9">
          <?php
          $_menuHtml = array();
          if(@$menufront->menu_name && $menufront->menu_name != null){
            $_menufront = explode('||',$menufront->menu_name);
            if(count($_menufront) > 0){
              foreach($_menufront as $r){
                $_r = explode('|',$r);
                $_menuHtml[$_r[0]] = (@$_r[1])?$_r[1]:'';
              };
            }
          }
          ?>
          <?php foreach($langs as $lang){ ?>
            <input type="hidden" name="menu_lang[<?=$lang['lang_kode']?>]" value="<?=$lang['lang_kode']?>">
            <input type="text" class="form-control form-control-lg" placeholder="Menu Name [<?=$lang['lang_kode']?>]" name="menu_name[<?=$lang['lang_kode']?>]" value="<?=(@$_menuHtml[$lang['lang_kode']])?$_menuHtml[$lang['lang_kode']]:'';?>">
          <?php } ?>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label col-form-label-lg">Slug</label>
        <div class="col-sm-9">
          <?php
          $_slugHtml = array();
          if(@$menufront->slug && $menufront->slug != null){
            $_slug = explode('||',$menufront->slug);
            if(count($_slug) > 0){
              foreach($_slug as $r){
                $_r = explode('|',$r);
                $_slugHtml[$_r[0]] = (@$_r[1])?$_r[1]:'';
              };
            }
          }
          ?>
          <?php foreach($langs as $lang){ ?>
            <input type="text" class="form-control form-control-lg" placeholder="Slug / URL [<?=$lang['lang_kode']?>]" name="menu_slug[<?=$lang['lang_kode']?>]" value="<?=(@$_slugHtml[$lang['lang_kode']])?$_slugHtml[$lang['lang_kode']]:'';?>">
          <?php } ?>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label col-form-label-lg">Order</label>
        <div class="col-sm-9">
          <input type="text" class="form-control form-control-lg" placeholder="Order" name="menu_order" value="{{ $menufront->menu_order }}">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label col-form-label-lg">Description</label>
        <div class="col-sm-9">
          <textarea class="form-control form-control-lg" placeholder="Description" name="menu_desc">{{ $menufront->menu_desc }}</textarea>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label col-form-label-lg">Show</label>
        <div class="col-sm-9">
          <select class="form-control form-control-lg" name="menu_is_show">
            <option value="Y" @if ($menufront->menu_is_show == 'Y') selected @endif>Yes</option>
            <option value="N" @if ($menufront->menu_is_show == 'N') selected @endif>No</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label col-form-label-lg">Icon</label>
        <div class="col-sm-9">
          <input type="text" class="form-control form-control-lg" placeholder="Icon" name="menu_icon_large" value="{{ $menufront->menu_icon_large }}">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label col-form-label-lg">Icon Small</label>
        <div class="col-sm-9">
          <input type="text" class="form-control form-control-lg" placeholder="Icon Small" name="menu_icon_small" value="{{ $menufront->menu_icon_small }}">
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button id="btnSubmit" type="button" class="btn btn-primary">Save changes</button>
  </div>
  </form>
</div>
<script>
  $('#btnSubmit').on('click', function () {
    var frm = $('#formModalAdd');
    $.ajax({
      type: frm.attr('method'),
      url: frm.attr('action'),
      data: frm.serialize(),
      dataType: "json",
      success: function (data) {
        if(data == true) {
          $('#ModalLaravelAdd').modal('hide');
          swal(
            'Success!',
            'Your data has been saved.',
            'success'
          );
          table.ajax.reload();
        }else{
          swal(
            'Failed!',
            'Your data not saved :)',
            'error'
          )
        }
      },
      error: function (data) {
        console.log(data);
      }
    });
  });
</script>
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="ModalLaravelAddLabel">View Category</h4>
  </div>
  <div class="modal-body">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-lg">Category Code</label>
      <div class="col-sm-9">
        {{$category->category_kode}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-lg">Slug</label>
      <div class="col-sm-9">
        <?php
        if(@$category->slug && $category->slug != null){
          $_slug = explode('|',$category->slug);
          $_slugHtml = '';
          if(count($_slug) > 0){
            $i = 0;
            foreach($_slug as $r){
              $_slugHtml .= ($i==0)?$r:'<br>'.$r;
              $i++;
            };
          }
          echo $category->slug = $_slugHtml;
        }
        ?>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-lg">Category Label</label>
      <div class="col-sm-9">
        <?php
        if(@$category->label && $category->label != null){
          $_label = explode('|',$category->label);
          $_labelHtml = '';
          if(count($_label) > 0){
            $i = 0;
            foreach($_label as $r){
              $_labelHtml .= ($i==0)?$r:'<br>'.$r;
              $i++;
            };
          }
          echo $category->label = $_labelHtml;
        }
        ?>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Created</label>
      <div class="col-sm-9">
        {{$category->created_at}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Updated</label>
      <div class="col-sm-9">
        {{$category->updated_at}}
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>
@extends('layouts.app')

@section('style')
<link href="{{ asset('plugin/jsTree/themes/default/style.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Add Group
				</div>
				<form id="formModalAdd" method="post" action="{{ url('group') }}">
				{{csrf_field()}}
				<input type="hidden" name="menu_id1" id="menu_id">
				<div class="panel-body">
					<div id="menu_list">
						<?php echo $menus?>
					</div>
					<?php echo $actions?>
				</div>
				<div class="panel-footer text-right">
					<a href="{{url('group')}}" class="btn btn-default">Cancle</a>
					<button id="btnSubmit" type="button" class="btn btn-primary">Save</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="{{ asset('plugin/jsTree/jstree.min.js') }}"></script>
<script>
  	$('#btnSubmit').on('click', function () {
    	var frm = $('#formModalAdd');
		var checked_ids = []; 
        $("#menu_list").find(".jstree-undetermined").each(function(i,element){            
            checked_ids.push($(element).attr("id"));
        
            if ($(this).find(".jstree-undetermined").length == 0) {  
                $(this).find(".jstree-checked").each(function(i, element){          
                    checked_ids.push($(element).attr("id"));
                });         
            }
        });

        $("#menu_list").find(".jstree-checked").each(function(i, element){
            var id = $(element).attr("id");
            if (!inArray(id, checked_ids)) {
                checked_ids.push(id);
            }        
        }); 

        alert(checked_ids);

        $('#menu_id').val(checked_ids);

		// $.ajax({
		// 	type: frm.attr('method'),
		// 	url: frm.attr('action'),
		// 	data: new FormData($('#formModalAdd')[0]),
		// 	dataType: 'JSON',
		// 	success: function (data) {
		// 	if(data == true) {
		// 		swal({
		// 			title: 'Success!',
		// 			text: 'Your data has been saved.',
		// 			type: 'success',
		// 			showCancelButton: false,
		// 			confirmButtonColor: '#3085d6',
		// 			}).then(function () {
		// 				window.location = "{{ url('post') }}";
		// 			});
		// 		}else{
		// 			swal(
		// 				'Failed!',
		// 				'Your data not saved :)',
		// 				'error'
		// 			)
		// 		}
		// 	},
		// 	error: function (data) {
		// 		console.log(data);
		// 	}
		// });
	});

    $("#menu_list").jstree({
    	"checkbox" : {
    		"keep_selected_style" : false
    	},
    	"plugins" : [ "checkbox" ]
    });

    function inArray(needle, haystack) {
	    var length = haystack.length;
	    for(var i = 0; i < length; i++) {
	        if(haystack[i] == needle) return true;
	    }
	    return false;
	}
</script>
@endsection
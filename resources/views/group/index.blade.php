@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Group
          <div class="btn-group pull-right">
            <a href="{{url('group/create')}}" class="btn btn-xs btn-success">add</a>
          </div>
          <span class="clearfix"></span>
        </div>
        <div class="panel-body">
          <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="data-table">
            <thead>
              <tr>
                <th rowspan="2">Name</th>
                <th rowspan="2">Description</th>
                <th rowspan="2">Created</th>
                <th rowspan="2">Updated</th>
                <th colspan="3" width="15%">Action</th>
              </tr>
              <tr>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  var table;
  $(function() {
    table = $('#data-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! url('groupanydata') !!}',
      columns: [
        { data: 'group_name', name: 'group_name' },
        { data: 'group_desc', name: 'group_desc' },
        { data: 'created_at', name: 'created_at' },
        { data: 'updated_at', name: 'updated_at' },
        {
          data: "group_id",
          orderable: false,
          mRender: function (group_id) {
            return '<a href="{{url('group')}}/'+group_id+'" class="btn btn-xs btn-primary">View</a>';
          }
        },
        {
          data: "group_id",
          orderable: false,
          mRender: function (group_id) {
            return '<a href="{{url('group')}}/'+group_id+'/edit" class="btn btn-xs btn-warning">Edit</a>';
          }
        },
        {
          data: "group_id",
          orderable: false,
          mRender: function (group_id) {
            return '<form id="form-'+group_id+'" action="{{url('group')}}/'+group_id+'" method="post">'
              + '{{csrf_field()}}'
              + '<input name="_method" type="hidden" value="DELETE">'
              + '<button class="btn btn-xs btn-danger" type="button" onclick="del('+group_id+')">Delete</button>'
            + '</form>';
          }
        }
      ]
    });
  });
</script>
@endsection
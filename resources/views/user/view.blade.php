<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="ModalLaravelAddLabel">View User</h4>
  </div>
  <div class="modal-body">
    <div class="form-group row">
      <label class="col-sm-2 col-form-label col-form-label-lg">Name</label>
      <div class="col-sm-10">
        {{$user->name}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label col-form-label-sm">Email</label>
      <div class="col-sm-10">
        {{$user->email}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label col-form-label-sm">Created</label>
      <div class="col-sm-10">
        {{$user->created_at}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label col-form-label-sm">Updated</label>
      <div class="col-sm-10">
        {{$user->updated_at}}
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	User
                	<div class="btn-group pull-right">
                		<a href="#" class="btn btn-xs btn-success" onclick="formModalAdd('{{url('user/create')}}')">add</a>
                	</div>
                	<span class="clearfix"></span>
                </div>
                <div class="panel-body">
					<table class="table table-striped table-bordered" cellspacing="0" width="100%" id="data-table">
						<thead>
							<tr>
								<th rowspan="2">Name</th>
								<th rowspan="2">Email</th>
								<th rowspan="2">Created</th>
								<th rowspan="2">Updated</th>
								<th colspan="3" width="15%">Action</th>
							</tr>
							<tr>
								<th>View</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
					</table>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
  var table;
  $(function() {
    table = $('#data-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! url('useranydata') !!}',
      columns: [
        { data: 'name', name: 'name' },
        { data: 'email', name: 'email' },
        { data: 'created_at', name: 'created_at' },
        { data: 'updated_at', name: 'updated_at' },
        {
          data: "id",
          orderable: false,
          mRender: function (id) {
            return '<a href="#" class="btn btn-xs btn-primary" onclick="formModalAdd(\'{{url('user')}}/'+id+'\')">View</a>';
          }
        },
        {
          data: "id",
          orderable: false,
          mRender: function (id) {
            return '<a href="#" class="btn btn-xs btn-warning" onclick="formModalAdd(\'{{url('user')}}/'+id+'/edit\')">Edit</a>';
          }
        },
        {
          data: "id",
          orderable: false,
          mRender: function (id) {
            return '<form id="form-'+id+'" action="{{url('user')}}/'+id+'" method="post">'
              + '{{csrf_field()}}'
              + '<input name="_method" type="hidden" value="DELETE">'
              + '<button class="btn btn-xs btn-danger" type="button" onclick="del('+id+')">Delete</button>'
            + '</form>';
          }
        }
      ]
    });
  });
</script>
@endsection
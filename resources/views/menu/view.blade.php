<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="ModalLaravelAddLabel">View Menu</h4>
  </div>
  <div class="modal-body">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-lg">Parent</label>
      <div class="col-sm-9">
        {{$menu->parent_name}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Menu Name</label>
      <div class="col-sm-9">
        <?php
        if(@$menu->menu_name && $menu->menu_name != null){
          $_menu = explode('|',$menu->menu_name);
          $_menuHtml = '';
          if(count($_menu) > 0){
            $i = 0;
            foreach($_menu as $r){
              $_menuHtml .= ($i==0)?$r:'<br>'.$r;
              $i++;
            };
          }
          echo $menu->menu_name = $_menuHtml;
        }
        ?>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Slug</label>
      <div class="col-sm-9">
		    <?php
        if(@$menu->slug && $menu->slug != null){
          $_slug = explode('|',$menu->slug);
          $_slugHtml = '';
          if(count($_slug) > 0){
            $i = 0;
            foreach($_slug as $r){
              $_slugHtml .= ($i==0)?$r:'<br>'.$r;
              $i++;
            };
          }
          echo $menu->slug = $_slugHtml;
        }
        ?>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Order</label>
      <div class="col-sm-9">
        {{$menu->menu_order}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Description</label>
      <div class="col-sm-9">
        {{$menu->menu_desc}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Show</label>
      <div class="col-sm-9">
        {{$menu->menu_is_show}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Icon</label>
      <div class="col-sm-9">
        {{$menu->menu_icon_large}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Icon Small</label>
      <div class="col-sm-9">
        {{$menu->menu_icon_small}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-lg">Action</label>
      <div class="col-sm-9">
        <?php
        if(@$menu->actions && $menu->actions != null){
          $_actions = explode(',',$menu->actions);
          $_actionsHtml = array();
          if(count($_actions) > 0){
            foreach($_actions as $r){
              $_actionsHtml[$r] = $r;
            };
          }
        }
        ?>
        <ol>
        <?php foreach($actions as $action){ 
          if(@$menu->actions && $menu->actions != null){
            if(array_search($action->action_id,$_actionsHtml)){ ?>
              <li><?=$action->label?></li>
            <?php }
          }
        } ?>
        </ol>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Created</label>
      <div class="col-sm-9">
        {{$menu->created_at}}
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label col-form-label-sm">Updated</label>
      <div class="col-sm-9">
        {{$menu->updated_at}}
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>
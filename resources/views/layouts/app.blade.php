<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- plugin -->
    <link href="{{ asset('plugin/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('plugin/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('plugin/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <!-- custom -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    @yield('style')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ route('home') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">CMS <b class="caret"></b></a> 
                                <ul class="dropdown-menu">
                                    <li class="dropdown dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">User</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url('group') }}">Group</a></li>
                                            <li><a href="{{ url('user') }}">User</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Setting</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url('lang') }}">Language</a></li>
                                            <li><a href="{{ url('action') }}">Action</a></li>
                                            <li><a href="{{ url('setting') }}">Attribute</a></li>
                                            <li><a href="{{ url('menu') }}">Menu</a></li>
                                            <li><a href="{{ url('crud') }}">CRUD</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Master Data</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url('category') }}">Category</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Front Page</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url('post') }}">Post</a></li>
                                            <li><a href="{{ url('menufront') }}">Front Menu</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown Link 5</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Dropdown Submenu Link 5.1</a></li>
                                            <li><a href="#">Dropdown Submenu Link 5.2</a></li>
                                            <li><a href="#">Dropdown Submenu Link 5.3</a></li>
                                            <li class="dropdown dropdown-submenu pull-left">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown Submenu Link 5.4</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Dropdown Submenu Link 5.4.1</a></li>
                                                    <li><a href="#">Dropdown Submenu Link 5.4.2</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li><a href="{{ url('/') }}">Home</a></li>
                                    <li><a href="#" onclick="formModalAdd('{{ route('profile', ['id' => Auth::user()->id]) }}')">Profile</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Modal -->
    <div class="modal fade" id="ModalLaravelAdd" tabindex="-1" role="dialog" aria-labelledby="ModalLaravelAddLabel">
        <div class="modal-dialog" role="document">
            <div id="ModalLaravelAddContent"></div>
        </div>
    </div>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <!-- plugin -->
    <script src="{{ asset('plugin/bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ asset('plugin/sweetalert/sweetalert.js') }}"></script>
    <script src="{{ asset('plugin/datatables/datatables.js') }}"></script>
    <script src="{{ asset('plugin/datatables/dataTables.bootstrap4.js') }}"></script>

    @yield('script')

    <script type="text/javascript">
        function del(id){
            var frm = $('#form-' + id);
            swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then(function () {
              $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (data) {
                  if(data == true) {
                    swal(
                      'Deleted!',
                      'Your data has been deleted.',
                      'success'
                    );
                    table.ajax.reload();
                  }else{
                    swal(
                      'Delete Failed!',
                      'Your data not deleted :)',
                      'error'
                    )
                  }
                },
                error: function (data) {
                  console.log(data);
                }
              });
            });
        }

        function sts(id){
            var frm = $('#form-' + id);
            swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, do it!'
            }).then(function () {
              $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (data) {
                  if(data == true) {
                    swal(
                      'Success!',
                      'Your data has been changed.',
                      'success'
                    );
                    table.ajax.reload();
                  }else{
                    swal(
                      'Update Failed!',
                      'Your data not changed :)',
                      'error'
                    )
                  }
                },
                error: function (data) {
                  console.log(data);
                }
              });
            });
        }

        function formModalAdd(url){
            $('#ModalLaravelAdd').modal('show');
            $.ajax({
                type: "GET",
                url: url,
                success: function (data) {
                    $('#ModalLaravelAddContent').html(data);
                }
            });
        }
    </script>
</body>
</html>

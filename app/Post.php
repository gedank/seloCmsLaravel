<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $table = 'cms_post';
    protected $fillable = ['post_id','post_category_kode','post_keyword','post_img','post_is_show','user_id'];
    protected $primaryKey = 'post_id';
}

class Post_text extends Model
{
	protected $table = 'cms_post_text';
    protected $fillable = ['post_id','post_lang','post_title','post_post','slug'];
    protected $primaryKey = 'post_id';
    public $timestamps = false;
}
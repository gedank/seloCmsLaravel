<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
	protected $table = 'cms_setting';
    protected $fillable = ['setting_id','setting_name','setting_value','setting_desc','user_id'];
    protected $primaryKey = 'setting_id';
}
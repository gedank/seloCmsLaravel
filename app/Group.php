<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
	protected $table = 'cms_group';
    protected $fillable = ['group_id','group_name','group_desc','user_id'];
    protected $primaryKey = 'group_id';
}

class Group_menu extends Model
{
	protected $table = 'cms_group_menu';
    protected $fillable = ['groupmenu_group_id','groupmenu_menu_id'];
    protected $primaryKey = 'groupmenu_group_id';
    public $timestamps = false;
}
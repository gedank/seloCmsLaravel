<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'cms_category';
    protected $fillable = ['category_id','category_kode','user_id'];
    protected $primaryKey = 'category_id';
}

class Category_text extends Model
{
	protected $table = 'cms_category_text';
    protected $fillable = ['category_id','category_lang','category_label','slug'];
    protected $primaryKey = 'category_id';
    public $timestamps = false;
}
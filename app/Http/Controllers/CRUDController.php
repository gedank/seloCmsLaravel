<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crud;
use Yajra\Datatables\Datatables;

class CRUDController extends Controller
{
    public function index()
    {
        return view('crud.index');
    }

    public function anyData()
    {
        return Datatables::of(Crud::query())->make(true);
    }

    public function create()
    {
        return view('crud.create');
    }

    public function store(Request $request)
    {
        $crud = new Crud([
          'title' => $request->get('title'),
          'post' => $request->get('post')
        ]);
        if($crud->save()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function show($id)
    {
        $crud = Crud::find($id);
        return view('crud.view', compact('crud','id'));
    }

    public function edit($id)
    {
        $crud = Crud::find($id);
        return view('crud.edit', compact('crud','id'));
    }

    public function update(Request $request, $id)
    {
        $crud = Crud::find($id);
        $crud->title = $request->get('title');
        $crud->post = $request->get('post');
        if($crud->save()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy($id)
    {
        $crud = Crud::find($id);
        if($crud->delete()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }
}
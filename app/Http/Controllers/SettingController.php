<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Yajra\Datatables\Datatables;

class SettingController extends Controller
{
    public function index()
    {
        return view('setting.index');
    }

    public function anyData()
    {
        return Datatables::of(Setting::query())->make(true);
    }

    public function create()
    {
        return view('setting.create');
    }

    public function store(Request $request)
    {
        $setting = new Setting([
          'setting_name' => $request->get('setting_name'),
          'setting_value' => $request->get('setting_value'),
          'setting_desc' => $request->get('setting_desc')
        ]);
        if($setting->save()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function show($id)
    {
        $setting = Setting::find($id);
        return view('setting.view', compact('setting','id'));
    }

    public function edit($id)
    {
        $setting = Setting::find($id);
        return view('setting.edit', compact('setting','id'));
    }

    public function update(Request $request, $id)
    {
        $setting = Setting::find($id);
        $setting->setting_name = $request->get('setting_name');
        $setting->setting_value = $request->get('setting_value');
        $setting->setting_desc = $request->get('setting_desc');
        if($setting->save()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy($id)
    {
        $setting = Setting::find($id);
        if($setting->delete()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Category_text;
use App\Lang;
use Yajra\Datatables\Datatables;
use DB;

class CategoryController extends Controller
{
    public function index()
    {
        return view('category.index');
    }

    public function anyData(Request $request)
    {
        $query = DB::select('SELECT 
            c.category_kode,
            c.created_at,
            c.updated_at,
            ct.category_id,
            ct.label,
            ct.slug
            FROM 
                (SELECT ct.category_id,
                    ct.category_label as label, 
                    ct.slug as slug 
                FROM cms_category_text as ct 
                WHERE ct.category_lang="'.$request->session()->get('lang').'") as ct 
            LEFT JOIN cms_category as c ON c.category_id=ct.category_id
            ');

        return Datatables::of($query)
            ->escapeColumns()
            ->make(true);
    }

    public function create()
    {
        $langs = Lang::all()->toArray();
        return view('category.create', compact('langs'));
    }

    public function store(Request $request)
    {
        $category_lang = $request->get('category_lang');
        $category_label = $request->get('category_label');
        $category = new Category([
          'category_kode' => $request->get('category_kode'),
          'user_id' => $request->get('user_id')
        ]);
        $category->save();
        $id = $category->category_id;
        foreach($category_lang as $r){
            $category_text = new Category_text([
              'category_id' => $id,
              'category_lang' => $r,
              'category_label' => $category_label[$r],
              'slug' => str_replace(' ','-',strtolower(preg_replace('/[^A-Za-z0-9\-]/','',$category_label[$r])))
            ]);
            $category_text->save();
        }
        if($id){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function show($id)
    {
        $query = DB::select('SELECT 
            c.category_kode,
            c.created_at,
            c.updated_at,
            ct.category_id,
            ct.label,
            ct.slug
            FROM 
                (SELECT ct.category_id,
                    GROUP_CONCAT(CONCAT(ct.category_lang," : ",ct.category_label) SEPARATOR "|") as label, 
                    GROUP_CONCAT(CONCAT(ct.category_lang," : ",ct.slug) SEPARATOR "|") as slug 
                FROM cms_category_text as ct 
                GROUP BY ct.category_id) as ct 
            LEFT JOIN cms_category as c ON c.category_id=ct.category_id 
            WHERE c.category_id='.$id.'
            ');
        $category = $query[0];
        return view('category.view', compact('category','id'));
    }

    public function edit($id)
    {
        $langs = Lang::all()->toArray();
        $query = DB::select('SELECT 
            c.category_kode,
            c.created_at,
            c.updated_at,
            ct.category_id,
            ct.label,
            ct.slug
            FROM 
                (SELECT ct.category_id,
                    GROUP_CONCAT(CONCAT(ct.category_lang,",",ct.category_label) SEPARATOR "|") as label, 
                    GROUP_CONCAT(CONCAT(ct.category_lang,",",ct.slug) SEPARATOR "|") as slug 
                FROM cms_category_text as ct 
                GROUP BY ct.category_id) as ct 
            LEFT JOIN cms_category as c ON c.category_id=ct.category_id 
            WHERE c.category_id='.$id.'
            ');
        $category = $query[0];
        return view('category.edit', compact('category','id','langs'));
    }

    public function update(Request $request, $id)
    {
        $category_lang = $request->get('category_lang');
        $category_label = $request->get('category_label');
        $category = Category::find($id);
        $category->category_kode = $request->get('category_kode');
        $category->user_id = $request->get('user_id');
        $category->save();
        $this->destroy_text($id);
        foreach($category_lang as $r){
            $category_text = new Category_text([
              'category_id' => $id,
              'category_lang' => $r,
              'category_label' => $category_label[$r],
              'slug' => str_replace(' ','-',strtolower(preg_replace('/[^A-Za-z0-9 \-]/','',$category_label[$r])))
            ]);
            $category_text->save();
        }
        if($id){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $this->destroy_text($id);
        if($category->delete()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy_text($id)
    {
        $category = Category_text::find($id);
        $category->delete();
    }
}
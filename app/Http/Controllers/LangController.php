<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lang;
use Yajra\Datatables\Datatables;

class LangController extends Controller
{
    public function index()
    {
        return view('lang.index');
    }

    public function anyData()
    {
        return Datatables::of(Lang::query())->make(true);
    }

    public function create()
    {
        return view('lang.create');
    }

    public function store(Request $request)
    {
        $lang = new Lang([
          'lang_kode' => $request->get('lang_kode'),
          'lang_name' => $request->get('lang_name'),
          'lang_icon' => $request->get('lang_icon')
        ]);
        if($lang->save()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function show($lang_id)
    {
        $lang = Lang::find($lang_id);
        return view('lang.view', compact('lang','lang_id'));
    }

    public function edit($id)
    {
        $lang = Lang::find($id);
        return view('lang.edit', compact('lang','id'));
    }

    public function update(Request $request, $id)
    {
        $lang = Lang::find($id);
        $lang->lang_kode = $request->get('lang_kode');
        $lang->lang_name = $request->get('lang_name');
        $lang->lang_icon = $request->get('lang_icon');
        if($lang->save()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy($id)
    {
        $lang = Lang::find($id);
        if($lang->delete()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }
}
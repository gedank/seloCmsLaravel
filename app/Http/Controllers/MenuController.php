<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\menu_text;
use App\menu_action;
use App\Lang;
use App\Action;
use Yajra\Datatables\Datatables;
use DB;

class MenuController extends Controller
{
    public function index()
    {
        return view('menu.index');
    }

    public function anyData(Request $request)
    {
        $query = DB::select('SELECT 
            m.menu_parent_id,
            m.menu_desc,
            m.menu_is_show,
            m.menu_icon_small,
            m.menu_icon_large,
            m.menu_order,
            m.created_at,
            m.updated_at,
            mt.menu_id,
            mt.lang,
            mt.name,
            mt.slug,
            mt2.menu_name as parent_name
            FROM 
                (SELECT mt.menu_id,
                    mt.menu_lang as lang, 
                    mt.menu_name as name,
                    mt.slug as slug 
                FROM cms_menu_text as mt 
                WHERE mt.menu_lang="'.$request->session()->get('lang').'" 
                AND mt.menu_id!=0) as mt 
            LEFT JOIN cms_menu as m ON m.menu_id=mt.menu_id
            LEFT JOIN cms_menu_text as mt2 ON mt2.menu_id=m.menu_parent_id 
                AND mt2.menu_lang="'.$request->session()->get('lang').'" 
            ORDER BY m.menu_id ASC, m.menu_parent_id ASC
            ');

        return Datatables::of($query)
            ->escapeColumns()
            ->make(true);
    }

    public function create(Request $request)
    {
        $langs = Lang::all()->toArray();
        $actions = DB::select('SELECT 
            a.action_kode,
            a.created_at,
            a.updated_at,
            at.action_id,
            at.label
            FROM 
                (SELECT at.action_id,
                    at.action_label as label
                FROM cms_action_text as at 
                WHERE at.action_lang="'.$request->session()->get('lang').'") as at 
            LEFT JOIN cms_action as a ON a.action_id=at.action_id
            ORDER BY a.action_id ASC
            ');
        $_parents = DB::select('SELECT 
            m.menu_parent_id,
            mt.menu_id,
            mt.name
            FROM 
                (SELECT mt.menu_id,
                    mt.menu_lang as lang, 
                    mt.menu_name as name 
                FROM cms_menu_text as mt 
                WHERE mt.menu_lang="'.$request->session()->get('lang').'" 
                AND mt.menu_id!=0) as mt 
            LEFT JOIN cms_menu as m ON m.menu_id=mt.menu_id 
            ORDER BY m.menu_order ASC
            ');
        $_menus = array();
        if($_parents!=null){
            foreach($_parents as $r){
                $_menus[$r->menu_parent_id][$r->menu_id] = $r;
            }
        }
        $parents = $this->selectMenu($_menus,0,0);
        return view('menu.create', compact('langs','parents','actions'));
    }

    public function store(Request $request)
    {
        $menu_lang = $request->get('menu_lang');
        $menu_name = $request->get('menu_name');
        $menu_slug = $request->get('menu_slug');
        $menu_action = $request->get('menu_action');
        $menu = new Menu([
          'menu_parent_id' => $request->get('menu_parent_id'),
          'menu_desc' => $request->get('menu_desc'),
          'menu_is_show' => $request->get('menu_is_show'),
          'menu_icon_small' => $request->get('menu_icon_small'),
          'menu_icon_large' => $request->get('menu_icon_large'),
          'menu_order' => $request->get('menu_order'),
          'user_id' => $request->get('user_id')
        ]);
        $menu->save();
        $id = $menu->menu_id;
        foreach($menu_lang as $r){
            $menu_text = new menu_text([
              'menu_id' => $id,
              'menu_lang' => $r,
              'menu_name' => $menu_name[$r],
              'slug' => str_replace(' ','-',strtolower(preg_replace('/[^A-Za-z0-9 \-]/','',$menu_slug[$r])))
            ]);
            $menu_text->save();
        }
        if($menu_action != null){
            foreach($menu_action as $r){
                $menu_action = new menu_action([
                  'menuaction_menu_id' => $id,
                  'menuaction_action_id' => $r
                ]);
                $menu_action->save();
            }
        }
        if($id){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function show(Request $request, $id)
    {
        $langs = Lang::all()->toArray();
        $actions = DB::select('SELECT 
            a.action_kode,
            a.created_at,
            a.updated_at,
            at.action_id,
            at.label
            FROM 
                (SELECT at.action_id,
                    at.action_label as label
                FROM cms_action_text as at 
                WHERE at.action_lang="'.$request->session()->get('lang').'") as at 
            LEFT JOIN cms_action as a ON a.action_id=at.action_id
            ORDER BY a.action_id ASC
            ');
        $query = DB::select('SELECT 
            GROUP_CONCAT(ma.menuaction_action_id SEPARATOR ",") as actions,
            mt2.menu_name as parent_name,
            m.menu_desc,
            m.menu_is_show,
            m.menu_icon_small,
            m.menu_icon_large,
            m.menu_order,
            m.created_at,
            m.updated_at,
            mt.menu_id,
            mt.menu_name,
            mt.slug
            FROM 
                (SELECT mt.menu_id,
                    GROUP_CONCAT(CONCAT(mt.menu_lang," : ",mt.menu_name) SEPARATOR "|") as menu_name, 
                    GROUP_CONCAT(CONCAT(mt.menu_lang," : ",mt.slug) SEPARATOR "|") as slug 
                FROM cms_menu_text as mt 
                GROUP BY mt.menu_id) as mt 
            LEFT JOIN cms_menu as m ON m.menu_id=mt.menu_id 
            LEFT JOIN cms_menu_text as mt2 ON mt2.menu_id=m.menu_parent_id 
                AND mt2.menu_lang="'.$request->session()->get('lang').'" 
            LEFT JOIN cms_menu_action as ma ON ma.menuaction_menu_id=m.menu_id 
            WHERE m.menu_id='.$id.' 
            GROUP BY m.menu_id,
            mt2.menu_name,
            m.menu_desc,
            m.menu_is_show,
            m.menu_icon_small,
            m.menu_icon_large,
            m.menu_order,
            m.created_at,
            m.updated_at,
            mt.menu_id,
            mt.menu_name,
            mt.slug
            ');
        $menu = $query[0];
        return view('menu.view', compact('menu','id','langs','actions'));
    }

    public function edit(Request $request, $id)
    {
        $langs = Lang::all()->toArray();
        $actions = DB::select('SELECT 
            a.action_kode,
            a.created_at,
            a.updated_at,
            at.action_id,
            at.label
            FROM 
                (SELECT at.action_id,
                    at.action_label as label
                FROM cms_action_text as at 
                WHERE at.action_lang="'.$request->session()->get('lang').'") as at 
            LEFT JOIN cms_action as a ON a.action_id=at.action_id
            ORDER BY a.action_id ASC
            ');
        $_parents = DB::select('SELECT 
            m.menu_parent_id,
            mt.menu_id,
            mt.name
            FROM 
                (SELECT mt.menu_id,
                    mt.menu_lang as lang, 
                    mt.menu_name as name 
                FROM cms_menu_text as mt 
                WHERE mt.menu_lang="'.$request->session()->get('lang').'" 
                AND mt.menu_id!=0) as mt 
            LEFT JOIN cms_menu as m ON m.menu_id=mt.menu_id 
            ORDER BY m.menu_order ASC
            ');
        $_menus = array();
        if($_parents!=null){
            foreach($_parents as $r){
                $_menus[$r->menu_parent_id][$r->menu_id] = $r;
            }
        }
        $parents = $this->selectMenu($_menus,0,0);
        $query = DB::select('SELECT 
            GROUP_CONCAT(ma.menuaction_action_id SEPARATOR ",") as actions,
            m.menu_parent_id,
            m.menu_desc,
            m.menu_is_show,
            m.menu_icon_small,
            m.menu_icon_large,
            m.menu_order,
            m.created_at,
            m.updated_at,
            mt.menu_id,
            mt.menu_name,
            mt.slug
            FROM 
                (SELECT mt.menu_id,
                    GROUP_CONCAT(CONCAT(mt.menu_lang,"|",mt.menu_name) SEPARATOR "||") as menu_name, 
                    GROUP_CONCAT(CONCAT(mt.menu_lang,"|",mt.slug) SEPARATOR "||") as slug 
                FROM cms_menu_text as mt 
                GROUP BY mt.menu_id) as mt 
            LEFT JOIN cms_menu as m ON m.menu_id=mt.menu_id 
            LEFT JOIN cms_menu_action as ma ON ma.menuaction_menu_id=m.menu_id 
            WHERE m.menu_id='.$id.'
            GROUP BY m.menu_parent_id,
            m.menu_desc,
            m.menu_is_show,
            m.menu_icon_small,
            m.menu_icon_large,
            m.menu_order,
            m.created_at,
            m.updated_at,
            mt.menu_id,
            mt.menu_name,
            mt.slug
            ');
        $menu = $query[0];
        return view('menu.edit', compact('menu','id','langs','parents','actions'));
    }

    public function update(Request $request, $id)
    {
        $menu_lang = $request->get('menu_lang');
        $menu_name = $request->get('menu_name');
        $menu_slug = $request->get('menu_slug');
        $menu_action = $request->get('menu_action');
        $Menu = Menu::find($id);
        $Menu->menu_parent_id = $request->get('menu_parent_id');
        $Menu->menu_desc = $request->get('menu_desc');
        $Menu->menu_is_show = $request->get('menu_is_show');
        $Menu->menu_icon_large = $request->get('menu_icon_large');
        $Menu->menu_icon_small = $request->get('menu_icon_small');
        $Menu->user_id = $request->get('user_id');
        $Menu->save();
        $this->destroy_text($id);
        foreach($menu_lang as $r){
            $menu_text = new menu_text([
              'menu_id' => $id,
              'menu_lang' => $r,
              'menu_name' => $menu_name[$r],
              'slug' => str_replace(' ','-',strtolower(preg_replace('/[^A-Za-z0-9 \-]/','',$menu_slug[$r])))
            ]);
            $menu_text->save();
        }
        if($menu_action != null){
            foreach($menu_action as $r){
                $menu_action = new menu_action([
                  'menuaction_menu_id' => $id,
                  'menuaction_action_id' => $r
                ]);
                $menu_action->save();
            }
        }
        if($id){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy($id)
    {
        $menu = Menu::find($id);
        $this->destroy_text($id);
        if($menu->delete()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy_text($id)
    {
        $menu_text = menu_text::find($id);
        $menu_text->delete();
        $menu_action = menu_action::find($id);
        if($menu_action != null){
            $menu_action->delete();
        }
    }

    public function showSts(Request $request, $id)
    {
        $menu = Menu::find($id);
        $menu->menu_is_show = $request->get('menu_is_show');
        if($menu->save()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function hideSts(Request $request, $id)
    {
        $menu = Menu::find($id);
        $menu->menu_is_show = $request->get('menu_is_show');
        if($menu->save()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    private function selectMenu($_menu,$parent,$is){
        $selectMenu = array();
        if(@$_menu[$parent]&&$_menu[$parent]!=null){
            $space = "";
            for($j=1;$j<=$is;$j++){
                $space .= "--";
            }
            $is++;
            foreach($_menu[$parent] as $r){
                $selectMenu[$parent.'-'.$r->menu_id] = array('id'=>$r->menu_id,'name'=>$space.' '.$r->name);
                if(@$_menu[$r->menu_id]&&$_menu[$r->menu_id]!=null){
                    $newSelectMenu = $this->selectMenu($_menu,$r->menu_id,$is);
                    $selectMenu = array_merge($selectMenu,$newSelectMenu);
                }
            }
        }
        return $selectMenu;
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Action;
use App\Action_text;
use App\Lang;
use Yajra\Datatables\Datatables;
use DB;

class ActionController extends Controller
{
    public function index()
    {
        return view('action.index');
    }

    public function anyData(Request $request)
    {
        $query = DB::select('SELECT 
            a.action_kode,
            a.created_at,
            a.updated_at,
            at.action_id,
            at.label
            FROM 
                (SELECT at.action_id,
                    at.action_label as label
                FROM cms_action_text as at 
                WHERE at.action_lang="'.$request->session()->get('lang').'") as at 
            LEFT JOIN cms_action as a ON a.action_id=at.action_id
            ');

        return Datatables::of($query)
            ->escapeColumns()
            ->make(true);
    }

    public function create()
    {
        $langs = Lang::all()->toArray();
        return view('action.create', compact('langs'));
    }

    public function store(Request $request)
    {
        $action_lang = $request->get('action_lang');
        $action_label = $request->get('action_label');
        $action = new Action([
          'action_kode' => $request->get('action_kode'),
          'user_id' => $request->get('user_id')
        ]);
        $action->save();
        $id = $action->action_id;
        foreach($action_lang as $r){
            $action_text = new Action_text([
              'action_id' => $id,
              'action_lang' => $r,
              'action_label' => $action_label[$r]
            ]);
            $action_text->save();
        }
        if($id){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function show($id)
    {
        $query = DB::select('SELECT 
            a.action_kode,
            a.created_at,
            a.updated_at,
            at.action_id,
            at.label
            FROM 
                (SELECT at.action_id,
                    GROUP_CONCAT(CONCAT(at.action_lang," : ",at.action_label) SEPARATOR "|") as label
                FROM cms_action_text as at 
                GROUP BY at.action_id) as at 
            LEFT JOIN cms_action as a ON a.action_id=at.action_id 
            WHERE a.action_id='.$id.'
            ');
        $action = $query[0];
        return view('action.view', compact('action','id'));
    }

    public function edit($id)
    {
        $langs = Lang::all()->toArray();
        $query = DB::select('SELECT 
            a.action_kode,
            a.created_at,
            a.updated_at,
            at.action_id,
            at.label
            FROM 
                (SELECT at.action_id,
                    GROUP_CONCAT(CONCAT(at.action_lang,",",at.action_label) SEPARATOR "|") as label
                FROM cms_action_text as at 
                GROUP BY at.action_id) as at 
            LEFT JOIN cms_action as a ON a.action_id=at.action_id 
            WHERE a.action_id='.$id.'
            ');
        $action = $query[0];
        return view('action.edit', compact('action','id','langs'));
    }

    public function update(Request $request, $id)
    {
        $action_lang = $request->get('action_lang');
        $action_label = $request->get('action_label');
        $action = Action::find($id);
        $action->action_kode = $request->get('action_kode');
        $action->user_id = $request->get('user_id');
        $action->save();
        $this->destroy_text($id);
        foreach($action_lang as $r){
            $action_text = new Action_text([
              'action_id' => $id,
              'action_lang' => $r,
              'action_label' => $action_label[$r]
            ]);
            $action_text->save();
        }
        if($id){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy($id)
    {
        $action = Action::find($id);
        $this->destroy_text($id);
        if($action->delete()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy_text($id)
    {
        $action = Action_text::find($id);
        $action->delete();
    }
}
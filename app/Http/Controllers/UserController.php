<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
        $users = User::all()->toArray();        
        return view('user.index', compact('users'));
    }

    public function anyData()
    {
        return Datatables::of(User::query())->make(true);
    }

    public function profile($id)
    {
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        $msg = false;
        if($request->get('password') == $request->get('password_confirmation'))
        {
            $user = new User([
              'name' => $request->get('name'),
              'email' => $request->get('email'),
              'password' => bcrypt($request->get('password'))
            ]);
            if($user->save()){
                $msg = true;
            }
        }
        return \Response::json($msg);
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('user.view', compact('user','id'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('user.edit', compact('user','id'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        if($user->save()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        if($user->delete()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }
}
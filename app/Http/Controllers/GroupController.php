<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\Group_menu;
use App\Menu;
use App\Menu_text;
use Yajra\Datatables\Datatables;
use DB;

class GroupController extends Controller
{
    public function index()
    {
        return view('group.index');
    }

    public function anyData(Request $request)
    {
        $query = DB::select('SELECT 
            g.*
            FROM 
                cms_group g
            ');

        return Datatables::of($query)
            ->escapeColumns()
            ->make(true);
    }

    public function create(Request $request)
    {
        $d_menu   = array();
        $d_action = array();
        $_actions = DB::select('SELECT 
            at.action_id as id,
            at.label
            FROM 
                (SELECT at.action_id,
                    at.action_label as label
                FROM cms_action_text as at 
                WHERE at.action_lang="'.$request->session()->get('lang').'") as at 
            LEFT JOIN cms_action as a ON a.action_id=at.action_id
            ORDER BY a.action_id ASC
            ');
        $__actions = array();
        if($_actions!=null){
            foreach($_actions as $r){
                $__actions[$r->id] = $r->label;
            }
        }
        $_menus = DB::select('SELECT 
            GROUP_CONCAT(ma.menuaction_action_id SEPARATOR "|") as menu_action,
            m.menu_parent_id,
            mt.menu_id,
            mt.name
            FROM 
                (SELECT mt.menu_id,
                    mt.menu_lang as lang, 
                    mt.menu_name as name 
                FROM cms_menu_text as mt 
                WHERE mt.menu_lang="'.$request->session()->get('lang').'" 
                AND mt.menu_id!=0) as mt 
            LEFT JOIN cms_menu as m ON m.menu_id=mt.menu_id 
            LEFT JOIN cms_menu_action ma ON ma.menuaction_menu_id=m.menu_id 
            GROUP BY m.menu_id,
            m.menu_parent_id,
            mt.menu_id,
            mt.name
            ORDER BY m.menu_order ASC
            ');
        $__menus = array();
        if($_menus!=null){
            foreach($_menus as $r){
                $__menus[$r->menu_parent_id][$r->menu_id] = $r;
            }
        }
        $menus = $this->selectMenu($__menus,$d_menu,0,1,0,$__actions);
        $actions = $this->selectAction($__menus,$d_action,0,$__actions);
        return view('group.create', compact('menus','actions'));
    }

    public function store(Request $request)
    {
        $group_menu_id = $request->get('group_menu');
        $group = new Group([
          'group_name' => $request->get('group_name'),
          'group_desc' => $request->get('group_desc'),
          'user_id' => $request->get('user_id')
        ]);
        $group->save();
        $id = $group->group_id;
        foreach($group_menu_id as $r){
            $group_menu = new Group_menu([
              'groupmenu_group_id' => $id,
              'groupmenu_menu_id' => $r
            ]);
            $group_menu->save();
        }
        if($id){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function show($id)
    {
        $query = DB::select('SELECT 
            a.action_kode,
            a.created_at,
            a.updated_at,
            at.action_id,
            at.label
            FROM 
                (SELECT at.action_id,
                    GROUP_CONCAT(CONCAT(at.action_lang," : ",at.action_label) SEPARATOR "|") as label
                FROM cms_action_text as at 
                GROUP BY at.action_id) as at 
            LEFT JOIN cms_action as a ON a.action_id=at.action_id 
            WHERE a.action_id='.$id.'
            ');
        $action = $query[0];
        return view('group.view', compact('action','id'));
    }

    public function edit($id)
    {
        $langs = Lang::all()->toArray();
        $query = DB::select('SELECT 
            a.action_kode,
            a.created_at,
            a.updated_at,
            at.action_id,
            at.label
            FROM 
                (SELECT at.action_id,
                    GROUP_CONCAT(CONCAT(at.action_lang,",",at.action_label) SEPARATOR "|") as label
                FROM cms_action_text as at 
                GROUP BY at.action_id) as at 
            LEFT JOIN cms_action as a ON a.action_id=at.action_id 
            WHERE a.action_id='.$id.'
            ');
        $action = $query[0];
        return view('group.edit', compact('action','id','langs'));
    }

    public function update(Request $request, $id)
    {
        $group_menu_id = $request->get('group_menu');
        $group = Group::find($id);
        $group->group_name = $request->get('group_name');
        $group->group_desc = $request->get('group_desc');
        $group->user_id = $request->get('user_id');
        $group->save();
        $this->destroy_text($id);
        foreach($group_menu_id as $r){
            $group_menu = new Group_menu([
              'groupmenu_group_id' => $id,
              'groupmenu_menu_id' => $r
            ]);
            $group_menu->save();
        }
        if($id){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy($id)
    {
        $group = Group::find($id);
        $this->destroy_text($id);
        if($group->delete()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy_text($id)
    {
        $group = Group_menu::find($id);
        $group->delete();
    }

    private function selectMenu($_menu,$d_menu,$parent,$i,$is,$__actions){
        $selectMenu = ($i==1)?'<ul>':'<ul class="sub">';
        if(@$_menu[$parent]&&$_menu[$parent]!=null){
            foreach($_menu[$parent] as $r){
                if(@$_menu[$r->menu_id]&&$_menu[$r->menu_id]!=null){
                    $class = '';
                }else{
                    $class = (@$d_menu[$r->menu_id])?'jstree-checked':'jstree-unchecked';
                }
                $selectMenu .= '<li id="' . $r->menu_id . '" class="' . $class . '">';
                $action = array();
                if($r->menu_action!=''){
                    $val = explode('|',$r->menu_action);
                    foreach($val as $r1){
                        $label = (@$__actions[$r1])?$__actions[$r1]:$r1;
                        $action[$r1] = $label;
                    }
                    $action = '(' . implode(', ',$action) . ')';
                    $selectMenu .= '<a href="#" data-menuid="' . $r->menu_id . '" class="check_label">' . $r->name . ' <span data-toggle="modal" data-original-title="Action" data-target="#data_' . $r->menu_id . '">' . $action . '</span></a>';
                }else{
                    $selectMenu .= '<a>' . $r->name . '</a>';
                }
                $i++;
                if(@$_menu[$r->menu_id]&&$_menu[$r->menu_id]!=null){
                    $is++;
                    $selectMenu .= $this->selectMenu($_menu,$d_menu,$r->menu_id,$i,$is,$__actions);
                }
                $selectMenu .= '</li>';
            }
        }
        $selectMenu .= '</ul>';
        return $selectMenu;
    }

    private function selectAction($_menu,$d_action,$parent,$__actions){
        $selectMenu = '';
        if(@$_menu[$parent]&&$_menu[$parent]!=null){
            foreach($_menu[$parent] as $r){
                $action = array();
                $actionModal = '<div style="padding-left: 50px;">';
                if($r->menu_action!=''){
                    $val = explode('|',$r->menu_action);
                    foreach($val as $r1){
                        $label = (@$__actions[$r1])?$__actions[$r1]:$r1;
                        $action[$r1] = $label;
                        $class2 = (@$d_action[$r->menu_id][$r1])?'checked':'';
                        $actionModal .= '<label class="checkbox"><input class="check_action check_action_' . $r->menu_id . '" id="check_' . $r->menu_id . '_' . $r1 . '" type="checkbox" name="menu_action[' . $r->menu_id . '][' . $r1 . ']" value="' . $r1 . '" ' . $class2 . '/> ' . $label . '</label>';
                    }
                    $action = '(' . implode(', ',$action) . ')';
                    $actionModal .= '</div>';
                    $selectMenu .= '<div id="data_' . $r->menu_id . '" class="modal fade my-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                                    <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" id="mySmallModalLabel">Action</h4>
                                            </div>
                                            <div class="modal-body">' . $actionModal . '</div>
                                        </div>
                                     </div>
                                </div>';
                }
                if(@$_menu[$r->menu_id]&&$_menu[$r->menu_id]!=null){
                    $selectMenu .= $this->selectAction($_menu,$d_action,$r->menu_id,$__actions);
                }
                $selectMenu .= '</li>';
            }
        }
        $selectMenu .= '</ul>';
        return $selectMenu;
    }
}
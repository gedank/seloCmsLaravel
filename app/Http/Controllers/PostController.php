<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Post_text;
use App\Lang;
use App\Http\Requests\UploadRequest;
use Yajra\Datatables\Datatables;
use DB;
use Image;

class PostController extends Controller
{
    public function index()
    {
        return view('post.index');
    }

    public function anyData(Request $request)
    {
        $query = DB::select('SELECT 
            ct.category_label as post_category_kode,
            p.created_at,
            p.updated_at,
            p.post_img,
            p.post_is_show,
            pt.post_id,
            pt.post,
            pt.slug
            FROM 
                (SELECT pt.post_id,
                    pt.post_title as post, 
                    pt.slug as slug 
                FROM cms_post_text as pt 
                WHERE pt.post_lang="'.$request->session()->get('lang').'") as pt 
            LEFT JOIN cms_post as p ON p.post_id=pt.post_id 
            LEFT JOIN cms_category as c ON c.category_kode=p.post_category_kode 
            LEFT JOIN cms_category_text as ct ON ct.category_id=c.category_id AND ct.category_lang="'.$request->session()->get('lang').'"
            ');

        return Datatables::of($query)
            ->escapeColumns()
            ->make(true);
    }

    public function create(Request $request)
    {
        $langs = Lang::all()->toArray();
        $categories = DB::select('SELECT 
            c.category_kode,
            c.created_at,
            c.updated_at,
            ct.category_id,
            ct.category_lang,
            ct.category_label
            FROM cms_category_text as ct  
            LEFT JOIN cms_category as c ON c.category_id=ct.category_id
            WHERE ct.category_lang="'.$request->session()->get('lang').'"
            ');
        return view('post.create', compact('langs','categories'));
    }

    public function store(UploadRequest $request)
    {
        $post_lang = $request->get('post_lang');
        $post_title = $request->get('post_title');
        $post_post = $request->get('post_post');
        $filename = "";
        if(@$request->photos && $request->photos!=null){
            foreach ($request->photos as $photo) {
                $filename = $photo->store('public/photos');
                Image::make(str_replace('public','storage',$filename))->resize(64, 64)->save(str_replace('public/photos','storage/photos/thumb',$filename));
            }
        }
        $post = new Post([
          'post_category_kode' => $request->get('post_category_kode'),
          'post_keyword' => $request->get('post_keyword'),
          'post_img' => str_replace('public/','',$filename),
          'user_id' => $request->get('user_id')
        ]);
        $post->save();
        $id = $post->post_id;
        foreach($post_lang as $r){
            $post_text = new Post_text([
              'post_id' => $id,
              'post_lang' => $r,
              'post_title' => $post_title[$r],
              'post_post' => $post_post[$r],
              'slug' => str_replace(' ','-',strtolower(preg_replace('/[^A-Za-z0-9\-]/','',$post_title[$r])))
            ]);
            $post_text->save();
        }
        if($id){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function show(Request $request, $id)
    {
        $langs = Lang::all()->toArray();
        $query = DB::select('SELECT 
            ct.category_label as post_category_kode,
            p.created_at,
            p.updated_at,
            p.post_img,
            p.post_keyword,
            p.post_is_show,
            pt.post_id,
            pt.post,
            pt.slug
            FROM 
                (SELECT pt.post_id,
                    GROUP_CONCAT(CONCAT(pt.post_lang,"|",pt.post_title,"|",pt.post_post) SEPARATOR "||") as post, 
                    GROUP_CONCAT(CONCAT(pt.post_lang,",",pt.slug) SEPARATOR "|") as slug 
                FROM cms_post_text as pt 
                GROUP BY pt.post_id) as pt 
            LEFT JOIN cms_post as p ON p.post_id=pt.post_id 
            LEFT JOIN cms_category as c ON c.category_kode=p.post_category_kode 
            LEFT JOIN cms_category_text as ct ON ct.category_id=c.category_id AND ct.category_lang="'.$request->session()->get('lang').'"
            WHERE p.post_id='.$id.'
            ');
        $post = $query[0];
        return view('post.view', compact('post','id','langs'));
    }

    public function edit(Request $request, $id)
    {
        $langs = Lang::all()->toArray();
        $categories = DB::select('SELECT 
            c.category_kode,
            c.created_at,
            c.updated_at,
            ct.category_id,
            ct.category_lang,
            ct.category_label
            FROM cms_category_text as ct  
            LEFT JOIN cms_category as c ON c.category_id=ct.category_id
            WHERE ct.category_lang="'.$request->session()->get('lang').'"
            ');
        $query = DB::select('SELECT 
            p.post_category_kode,
            p.created_at,
            p.updated_at,
            p.post_img,
            p.post_keyword,
            pt.post_id,
            pt.post,
            pt.slug
            FROM 
                (SELECT pt.post_id,
                    GROUP_CONCAT(CONCAT(pt.post_lang,"|",pt.post_title,"|",pt.post_post) SEPARATOR "||") as post, 
                    GROUP_CONCAT(CONCAT(pt.post_lang,",",pt.slug) SEPARATOR "|") as slug 
                FROM cms_post_text as pt 
                GROUP BY pt.post_id) as pt 
            LEFT JOIN cms_post as p ON p.post_id=pt.post_id 
            WHERE p.post_id='.$id.'
            ');
        $post = $query[0];
        return view('post.edit', compact('post','id','langs','categories'));
    }

    public function update(UploadRequest $request, $id)
    {
        $post_lang = $request->get('post_lang');
        $post_title = $request->get('post_title');
        $post_post = $request->get('post_post');
        $filename = "";
        if(@$request->photos && $request->photos!=null){
            foreach ($request->photos as $photo) {
                $filename = $photo->store('public/photos');
                Image::make(str_replace('public','storage',$filename))->resize(64, 64)->save(str_replace('public/photos','storage/photos/thumb',$filename));
            }
        }
        $post = Post::find($id);
        $post->post_category_kode = $request->get('post_category_kode');
        $post->post_keyword = $request->get('post_keyword');
        if($filename!=""){
            if($post->post_img != '' && $post->post_img != ''){
                unlink('storage/'.$post->post_img);
                unlink('storage/'.str_replace('photos','photos/thumb',$post->post_img));
            }
            $post->post_img = str_replace('public/','',$filename);
        }
        $post->user_id = $request->get('user_id');
        $post->save();
        $this->destroy_text($id);
        foreach($post_lang as $r){
            $post_text = new Post_text([
              'post_id' => $id,
              'post_lang' => $r,
              'post_title' => $post_title[$r],
              'post_post' => $post_post[$r],
              'slug' => str_replace(' ','-',strtolower(preg_replace('/[^A-Za-z0-9 \-]/','',$post_title[$r])))
            ]);
            $post_text->save();
        }
        if($id){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        if($post->post_img != '' && $post->post_img != ''){
            unlink('storage/'.$post->post_img);
            unlink('storage/'.str_replace('photos','photos/thumb',$post->post_img));
        }
        $this->destroy_text($id);
        if($post->delete()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy_text($id)
    {
        $post = Post_text::find($id);
        $post->delete();
    }

    public function showSts(Request $request, $id)
    {
        $post = Post::find($id);
        $post->post_is_show = $request->get('post_is_show');
        if($post->save()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function hideSts(Request $request, $id)
    {
        $post = Post::find($id);
        $post->post_is_show = $request->get('post_is_show');
        if($post->save()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }
}
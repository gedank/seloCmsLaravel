<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MenuFront;
use App\MenuFront_text;
use App\Lang;
use Yajra\Datatables\Datatables;
use DB;

class MenuFrontController extends Controller
{
    public function index()
    {
        return view('menufront.index');
    }

    public function anyData(Request $request)
    {
        $query = DB::select('SELECT 
            m.menu_parent_id,
            m.menu_desc,
            m.menu_is_show,
            m.menu_icon_small,
            m.menu_icon_large,
            m.menu_order,
            m.created_at,
            m.updated_at,
            mt.menu_id,
            mt.lang,
            mt.name,
            mt.slug,
            mt2.menu_name as parent_name
            FROM 
                (SELECT mt.menu_id,
                    mt.menu_lang as lang, 
                    mt.menu_name as name,
                    mt.slug as slug 
                FROM cms_menufront_text as mt 
                WHERE mt.menu_lang="'.$request->session()->get('lang').'" 
                AND mt.menu_id!=0) as mt 
            LEFT JOIN cms_menufront as m ON m.menu_id=mt.menu_id
            LEFT JOIN cms_menufront_text as mt2 ON mt2.menu_id=m.menu_parent_id 
                AND mt2.menu_lang="'.$request->session()->get('lang').'" 
            ORDER BY m.menu_order ASC
            ');

        return Datatables::of($query)
            ->escapeColumns()
            ->make(true);
    }

    public function create(Request $request)
    {
        $langs = Lang::all()->toArray();
        $_parents = DB::select('SELECT 
            m.menu_parent_id,
            mt.menu_id,
            mt.name
            FROM 
                (SELECT mt.menu_id,
                    mt.menu_lang as lang, 
                    mt.menu_name as name 
                FROM cms_menufront_text as mt 
                WHERE mt.menu_lang="'.$request->session()->get('lang').'" 
                AND mt.menu_id!=0) as mt 
            LEFT JOIN cms_menufront as m ON m.menu_id=mt.menu_id 
            ORDER BY m.menu_order ASC
            ');
        $_menus = array();
        if($_parents!=null){
            foreach($_parents as $r){
                $_menus[$r->menu_parent_id][$r->menu_id] = $r;
            }
        }
        $parents = $this->selectMenu($_menus,0,0);
        return view('menufront.create', compact('langs','parents'));
    }

    public function store(Request $request)
    {
        $menu_lang = $request->get('menu_lang');
        $menu_name = $request->get('menu_name');
        $menu_slug = $request->get('menu_slug');
        $menufront = new Menufront([
          'menu_parent_id' => $request->get('menu_parent_id'),
          'menu_desc' => $request->get('menu_desc'),
          'menu_is_show' => $request->get('menu_is_show'),
          'menu_icon_small' => $request->get('menu_icon_small'),
          'menu_icon_large' => $request->get('menu_icon_large'),
          'menu_order' => $request->get('menu_order'),
          'user_id' => $request->get('user_id')
        ]);
        $menufront->save();
        $id = $menufront->menu_id;
        foreach($menu_lang as $r){
            $menu_text = new Menufront_text([
              'menu_id' => $id,
              'menu_lang' => $r,
              'menu_name' => $menu_name[$r],
              'slug' => str_replace(' ','-',strtolower(preg_replace('/[^A-Za-z0-9 \-]/','',$menu_slug[$r])))
            ]);
            $menu_text->save();
        }
        if($id){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function show(Request $request, $id)
    {
        $langs = Lang::all()->toArray();
        $query = DB::select('SELECT 
            mt2.menu_name as parent_name,
            m.menu_desc,
            m.menu_is_show,
            m.menu_icon_small,
            m.menu_icon_large,
            m.menu_order,
            m.created_at,
            m.updated_at,
            mt.menu_id,
            mt.menu_name,
            mt.slug
            FROM 
                (SELECT mt.menu_id,
                    GROUP_CONCAT(CONCAT(mt.menu_lang," : ",mt.menu_name) SEPARATOR "|") as menu_name, 
                    GROUP_CONCAT(CONCAT(mt.menu_lang," : ",mt.slug) SEPARATOR "|") as slug 
                FROM cms_menufront_text as mt 
                GROUP BY mt.menu_id) as mt 
            LEFT JOIN cms_menufront as m ON m.menu_id=mt.menu_id 
            LEFT JOIN cms_menufront_text as mt2 ON mt2.menu_id=m.menu_parent_id 
                AND mt2.menu_lang="'.$request->session()->get('lang').'"
            WHERE m.menu_id='.$id.'
            ');
        $menufront = $query[0];
        return view('menufront.view', compact('menufront','id','langs'));
    }

    public function edit(Request $request, $id)
    {
        $langs = Lang::all()->toArray();
        $_parents = DB::select('SELECT 
            m.menu_parent_id,
            mt.menu_id,
            mt.name
            FROM 
                (SELECT mt.menu_id,
                    mt.menu_lang as lang, 
                    mt.menu_name as name 
                FROM cms_menufront_text as mt 
                WHERE mt.menu_lang="'.$request->session()->get('lang').'" 
                AND mt.menu_id!=0) as mt 
            LEFT JOIN cms_menufront as m ON m.menu_id=mt.menu_id 
            ORDER BY m.menu_order ASC
            ');
        $_menus = array();
        if($_parents!=null){
            foreach($_parents as $r){
                $_menus[$r->menu_parent_id][$r->menu_id] = $r;
            }
        }
        $parents = $this->selectMenu($_menus,0,0);
        $query = DB::select('SELECT 
            m.menu_parent_id,
            m.menu_desc,
            m.menu_is_show,
            m.menu_icon_small,
            m.menu_icon_large,
            m.menu_order,
            m.created_at,
            m.updated_at,
            mt.menu_id,
            mt.menu_name,
            mt.slug
            FROM 
                (SELECT mt.menu_id,
                    GROUP_CONCAT(CONCAT(mt.menu_lang,"|",mt.menu_name) SEPARATOR "||") as menu_name, 
                    GROUP_CONCAT(CONCAT(mt.menu_lang,"|",mt.slug) SEPARATOR "||") as slug 
                FROM cms_menufront_text as mt 
                GROUP BY mt.menu_id) as mt 
            LEFT JOIN cms_menufront as m ON m.menu_id=mt.menu_id 
            WHERE m.menu_id='.$id.'
            ');
        $menufront = $query[0];
        return view('menufront.edit', compact('menufront','id','langs','parents'));
    }

    public function update(Request $request, $id)
    {
        $menu_lang = $request->get('menu_lang');
        $menu_name = $request->get('menu_name');
        $menu_slug = $request->get('menu_slug');
        $menufront = MenuFront::find($id);
        $menufront->menu_parent_id = $request->get('menu_parent_id');
        $menufront->menu_desc = $request->get('menu_desc');
        $menufront->menu_is_show = $request->get('menu_is_show');
        $menufront->menu_icon_large = $request->get('menu_icon_large');
        $menufront->menu_icon_small = $request->get('menu_icon_small');
        $menufront->user_id = $request->get('user_id');
        $menufront->save();
        $this->destroy_text($id);
        foreach($menu_lang as $r){
            $menufront_text = new MenuFront_text([
              'menu_id' => $id,
              'menu_lang' => $r,
              'menu_name' => $menu_name[$r],
              'slug' => str_replace(' ','-',strtolower(preg_replace('/[^A-Za-z0-9 \-]/','',$menu_slug[$r])))
            ]);
            $menufront_text->save();
        }
        if($id){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy($id)
    {
        $menufront = MenuFront::find($id);
        $this->destroy_text($id);
        if($menufront->delete()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function destroy_text($id)
    {
        $menufront_text = MenuFront_text::find($id);
        $menufront_text->delete();
    }

    public function showSts(Request $request, $id)
    {
        $menufront = MenuFront::find($id);
        $menufront->menu_is_show = $request->get('menu_is_show');
        if($menufront->save()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    public function hideSts(Request $request, $id)
    {
        $menufront = MenuFront::find($id);
        $menufront->menu_is_show = $request->get('menu_is_show');
        if($menufront->save()){
            $msg = true;
        }else{
            $msg = false;
        }
        return \Response::json($msg);
    }

    private function selectMenu($_menu,$parent,$is){
        $selectMenu = array();
        if(@$_menu[$parent]&&$_menu[$parent]!=null){
            $space = "";
            for($j=1;$j<=$is;$j++){
                $space .= "--";
            }
            $is++;
            foreach($_menu[$parent] as $r){
                $selectMenu[$parent.'-'.$r->menu_id] = array('id'=>$r->menu_id,'name'=>$space.' '.$r->name);
                if(@$_menu[$r->menu_id]&&$_menu[$r->menu_id]!=null){
                    $newSelectMenu = $this->selectMenu($_menu,$r->menu_id,$is);
                    $selectMenu = array_merge($selectMenu,$newSelectMenu);
                }
            }
        }
        return $selectMenu;
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use General;
use Date;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session(['lang' => 'id']);
        $browser = General::getBrowser();
        $browser = Date::get_today();
        return view('home', compact('browser'));
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
	protected $table = 'cms_action';
    protected $fillable = ['action_id','action_kode','user_id'];
    protected $primaryKey = 'action_id';
}

class Action_text extends Model
{
	protected $table = 'cms_action_text';
    protected $fillable = ['action_id','action_lang','action_label'];
    protected $primaryKey = 'action_id';
    public $timestamps = false;
}
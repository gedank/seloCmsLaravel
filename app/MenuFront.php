<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuFront extends Model
{
	protected $table = 'cms_menufront';
    protected $fillable = ['menu_id','menu_parent_id','menu_desc','menu_is_show','menu_icon_small','menu_icon_large','menu_order','user_id'];
    protected $primaryKey = 'menu_id';
}

class MenuFront_text extends Model
{
	protected $table = 'cms_menufront_text';
    protected $fillable = ['menu_id','menu_lang','menu_name','slug'];
    protected $primaryKey = 'menu_id';
    public $timestamps = false;
}
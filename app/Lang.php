<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lang extends Model
{
	protected $table = 'cms_lang';
    protected $fillable = ['lang_id','lang_kode','lang_name','lang_icon'];
    protected $primaryKey = 'lang_id';
}
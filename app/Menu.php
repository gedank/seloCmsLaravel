<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	protected $table = 'cms_menu';
    protected $fillable = ['menu_id','menu_parent_id','menu_desc','menu_is_show','menu_icon_small','menu_icon_large','menu_order','user_id'];
    protected $primaryKey = 'menu_id';
}

class Menu_text extends Model
{
	protected $table = 'cms_menu_text';
    protected $fillable = ['menu_id','menu_lang','menu_name','slug'];
    protected $primaryKey = 'menu_id';
    public $timestamps = false;
}

class Menu_action extends Model
{
	protected $table = 'cms_menu_action';
    protected $fillable = ['menuaction_menu_id','menuaction_action_id'];
    protected $primaryKey = 'menuaction_menu_id';
    public $timestamps = false;
}
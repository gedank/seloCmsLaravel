/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.19-MariaDB : Database - db_laravel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_laravel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_laravel`;

/*Table structure for table `cms_action` */

CREATE TABLE `cms_action` (
  `action_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_kode` varchar(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`action_id`),
  UNIQUE KEY `action_kode` (`action_kode`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `cms_action` */

insert  into `cms_action`(`action_id`,`action_kode`,`user_id`,`created_at`,`updated_at`) values (1,'view',1,'2017-07-31 04:36:39','2017-07-31 04:36:39'),(2,'add',1,'2017-07-31 04:36:50','2017-07-31 04:36:50'),(3,'update',1,'2017-07-31 04:37:19','2017-07-31 04:37:19'),(4,'delete',1,'2017-07-31 04:37:40','2017-07-31 04:37:40'),(5,'approval',1,'2017-07-31 04:38:01','2017-07-31 04:38:01');

/*Table structure for table `cms_action_text` */

CREATE TABLE `cms_action_text` (
  `action_id` bigint(20) DEFAULT NULL,
  `action_lang` char(5) DEFAULT NULL,
  `action_label` varchar(255) DEFAULT NULL,
  UNIQUE KEY `action_index` (`action_id`,`action_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cms_action_text` */

insert  into `cms_action_text`(`action_id`,`action_lang`,`action_label`) values (3,'id','Ubah'),(3,'en','Update'),(4,'id','Hapus'),(4,'en','Delete'),(5,'id','Persetujuan'),(5,'en','Approval'),(2,'id','Tambah'),(2,'en','Add'),(1,'id','Lihat'),(1,'en','View');

/*Table structure for table `cms_category` */

CREATE TABLE `cms_category` (
  `category_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_kode` char(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cms_category` */

insert  into `cms_category`(`category_id`,`category_kode`,`user_id`,`created_at`,`updated_at`) values (1,'category',1,'2017-07-14 08:02:07','2017-07-14 08:02:07');

/*Table structure for table `cms_category_text` */

CREATE TABLE `cms_category_text` (
  `category_id` bigint(20) NOT NULL,
  `category_lang` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  UNIQUE KEY `category_text` (`category_id`,`category_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cms_category_text` */

insert  into `cms_category_text`(`category_id`,`category_lang`,`category_label`,`slug`) values (1,'en','Category','category'),(1,'id','Kategori','kategori');

/*Table structure for table `cms_group` */

CREATE TABLE `cms_group` (
  `group_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) NOT NULL,
  `group_desc` text,
  `user_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cms_group` */

/*Table structure for table `cms_group_menu` */

CREATE TABLE `cms_group_menu` (
  `groupmenu_group_id` bigint(20) DEFAULT NULL,
  `groupmenu_menu_id` bigint(20) DEFAULT NULL,
  UNIQUE KEY `group_index` (`groupmenu_group_id`,`groupmenu_menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cms_group_menu` */

/*Table structure for table `cms_lang` */

CREATE TABLE `cms_lang` (
  `lang_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `lang_kode` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`lang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cms_lang` */

insert  into `cms_lang`(`lang_id`,`lang_kode`,`lang_name`,`lang_icon`,`created_at`,`updated_at`) values (1,'id','Indonesia','id.png','2017-07-12 11:19:04','2017-07-12 11:19:07'),(2,'en','English','en.png','2017-07-12 11:19:31','2017-07-12 11:19:33');

/*Table structure for table `cms_menu` */

CREATE TABLE `cms_menu` (
  `menu_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `menu_parent_id` bigint(20) NOT NULL DEFAULT '0',
  `menu_desc` text COLLATE utf8mb4_unicode_ci,
  `menu_is_show` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_icon_small` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_icon_large` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_order` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cms_menu` */

insert  into `cms_menu`(`menu_id`,`menu_parent_id`,`menu_desc`,`menu_is_show`,`menu_icon_small`,`menu_icon_large`,`menu_order`,`user_id`,`created_at`,`updated_at`) values (1,0,NULL,'N',NULL,NULL,1,1,NULL,NULL),(2,0,NULL,'Y',NULL,NULL,1,1,'2017-07-31 06:30:03','2017-08-15 08:13:34'),(3,2,NULL,'Y',NULL,NULL,1,1,'2017-07-31 08:06:02','2017-08-15 08:13:31'),(4,2,NULL,'Y',NULL,NULL,2,1,'2017-08-15 07:47:16','2017-08-15 08:14:16'),(7,0,NULL,'N',NULL,NULL,2,1,'2017-08-15 07:52:48','2017-08-15 08:23:35'),(8,7,NULL,'Y',NULL,NULL,1,1,'2017-08-15 08:15:18','2017-08-15 08:15:18'),(9,2,NULL,'Y',NULL,NULL,3,1,'2017-08-15 08:31:58','2017-08-15 08:31:58'),(10,9,NULL,'Y',NULL,NULL,1,1,'2017-08-15 08:32:21','2017-08-15 08:32:21');

/*Table structure for table `cms_menu_action` */

CREATE TABLE `cms_menu_action` (
  `menuaction_menu_id` bigint(20) DEFAULT NULL,
  `menuaction_action_id` bigint(20) DEFAULT NULL,
  UNIQUE KEY `menu_index` (`menuaction_menu_id`,`menuaction_action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cms_menu_action` */

insert  into `cms_menu_action`(`menuaction_menu_id`,`menuaction_action_id`) values (3,1),(3,2),(3,3),(3,4),(3,5),(4,1),(4,2),(4,3),(4,4),(8,1),(8,2),(8,3),(8,4),(10,1),(10,2),(10,3),(10,4);

/*Table structure for table `cms_menu_text` */

CREATE TABLE `cms_menu_text` (
  `menu_id` bigint(20) NOT NULL,
  `menu_lang` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  UNIQUE KEY `menu_text` (`menu_id`,`menu_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cms_menu_text` */

insert  into `cms_menu_text`(`menu_id`,`menu_lang`,`menu_name`,`slug`) values (0,'en','-',NULL),(0,'id','-',NULL),(2,'en','Home',''),(2,'id','Beranda',''),(3,'en','Menu 1',''),(3,'id','Menu 1',''),(4,'en','Menu 2',''),(4,'id','Menu 2',''),(7,'en','Setting',''),(7,'id','Pengaturan',''),(8,'en','Menu 3',''),(8,'id','Menu 3',''),(9,'en','Menu 4',''),(9,'id','Menu 4',''),(10,'en','Menu 5',''),(10,'id','Menu 5','');

/*Table structure for table `cms_menufront` */

CREATE TABLE `cms_menufront` (
  `menu_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `menu_parent_id` bigint(20) NOT NULL DEFAULT '0',
  `menu_desc` text COLLATE utf8mb4_unicode_ci,
  `menu_is_show` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_icon_small` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_icon_large` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_order` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cms_menufront` */

insert  into `cms_menufront`(`menu_id`,`menu_parent_id`,`menu_desc`,`menu_is_show`,`menu_icon_small`,`menu_icon_large`,`menu_order`,`user_id`,`created_at`,`updated_at`) values (0,0,NULL,'N',NULL,NULL,1,1,NULL,'2017-07-24 15:47:06'),(1,0,'desc','Y','icon small','icon',1,1,'2017-07-24 08:42:37','2017-07-28 08:50:09'),(4,1,'desc','Y','icon small','icon',1,1,'2017-07-24 10:19:03','2017-08-15 08:06:25');

/*Table structure for table `cms_menufront_text` */

CREATE TABLE `cms_menufront_text` (
  `menu_id` bigint(20) NOT NULL,
  `menu_lang` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  UNIQUE KEY `menu_text` (`menu_id`,`menu_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cms_menufront_text` */

insert  into `cms_menufront_text`(`menu_id`,`menu_lang`,`menu_name`,`slug`) values (0,'en','-',NULL),(0,'id','-',NULL),(1,'en','Home','slug-en'),(1,'id','Beranda','slug-id'),(4,'en','Menu en','slug-en'),(4,'id','Menu id','slug-id');

/*Table structure for table `cms_post` */

CREATE TABLE `cms_post` (
  `post_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_category_kode` char(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_keyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_is_show` enum('N','Y') COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cms_post` */

insert  into `cms_post`(`post_id`,`post_category_kode`,`post_keyword`,`post_img`,`post_is_show`,`user_id`,`created_at`,`updated_at`) values (1,'category','laravel, crud','photos/aJpIGpKYWJqCILz52WkMLofxXWntxBXJeysggxqM.jpeg','N',1,'2017-07-14 08:33:50','2017-07-28 08:57:26');

/*Table structure for table `cms_post_text` */

CREATE TABLE `cms_post_text` (
  `post_id` bigint(20) NOT NULL,
  `post_lang` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_post` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  UNIQUE KEY `post_text` (`post_id`,`post_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cms_post_text` */

insert  into `cms_post_text`(`post_id`,`post_lang`,`post_title`,`post_post`,`slug`) values (1,'en','English Title?','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','english-title'),(1,'id','Judul Indonesia?','Jika anda sering menggunakan situs ini dan ingin membantu menjaganya agar tetap berada di internet, mohon pertimbangkan untuk menyumbang sejumlah kecil dana guna membantu membayar tagihan hosting dan bandwith. Tidak ada donasi minimal, jumlah berapapun akan sangat dihargai - klik disini untuk memberikan donasi dengan menggunakan PayPal. Terimakasih atas dukungan anda','judul-indonesia');

/*Table structure for table `cms_setting` */

CREATE TABLE `cms_setting` (
  `setting_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(255) NOT NULL,
  `setting_value` varchar(255) NOT NULL,
  `setting_desc` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`setting_id`),
  UNIQUE KEY `setting_name` (`setting_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `cms_setting` */

insert  into `cms_setting`(`setting_id`,`setting_name`,`setting_value`,`setting_desc`,`user_id`,`created_at`,`updated_at`) values (1,'view_per_page','50','Untuk membatasi limit pada setiap page pagination (example: 100 = 100 data yang ditampilkan setiap page)',NULL,'2017-07-28 16:36:53','2017-07-28 09:36:53');

/*Table structure for table `cruds` */

CREATE TABLE `cruds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cruds` */

insert  into `cruds`(`id`,`title`,`post`,`created_at`,`updated_at`) values (1,'Title','Post','2017-07-12 08:09:57','2017-07-12 08:09:57'),(2,'What is Lorem Ipsum?','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','2017-07-13 01:36:56','2017-07-13 01:36:56'),(3,'Why do we use it?','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,','2017-07-13 01:37:28','2017-07-13 01:37:28'),(4,'Where does it come from?','Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.','2017-07-13 01:37:51','2017-07-13 01:37:51'),(5,'Where can I get some?','There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,','2017-07-13 01:38:14','2017-07-13 01:38:14'),(6,'The standard Lorem Ipsum passage, used since the 1500s','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','2017-07-13 01:38:41','2017-07-13 01:38:41'),(7,'1914 translation by H. Rackham','But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.','2017-07-13 01:39:57','2017-07-13 01:39:57'),(8,'Lorem Ipsum','Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...','2017-07-13 01:40:42','2017-07-13 01:40:42'),(9,'1914 translation by H. Rackham 2017','To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it?','2017-07-13 01:41:10','2017-07-13 01:41:10'),(10,'The standard Lorem Ipsum passage, used since the 1500s 2017','Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2017-07-13 01:41:47','2017-07-13 01:41:47'),(11,'Why do we use it? 2017','Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text,','2017-07-13 01:42:19','2017-07-13 01:42:19');

/*Table structure for table `migrations` */

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (6,'2014_10_12_000000_create_users_table',1);

/*Table structure for table `password_resets` */

CREATE TABLE `password_resets` (
  `email` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `reset_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `password_resets` */

/*Table structure for table `users` */

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values (1,'hesti update','hesti@gmail.com','$2y$10$SkC.Q0tBbMqY/Kni8d4/xegCUOnbbR2s21RPfMyIPcGI5tRRqTzGS','uo6ZuZpMOqhC21wtbzWUIljrEq8zQsf33UOUplrCU3JF90XdeSfFw6d3m1AG','2017-07-04 07:13:03','2017-07-10 02:34:27');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

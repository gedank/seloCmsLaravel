<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('users', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('name');
        //     $table->string('email')->unique();
        //     $table->string('password');
        //     $table->string('cpassword');
        //     $table->rememberToken();
        //     $table->timestamps();
        // });
        // Schema::create('cruds', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('title');
        //     $table->string('post');
        //     $table->timestamps();
        // });
        // Schema::create('cms_lang', function (Blueprint $table) {
        //     $table->bigIncrements('lang_id');
        //     $table->char('lang_kode', 5);
        //     $table->string('lang_name');
        //     $table->string('lang_icon');
        //     $table->timestamps();
        // });
        // Schema::create('cms_category', function (Blueprint $table) {
        //     $table->bigIncrements('category_id');
        //     $table->char('category_kode', 15);$table->string('post_img');
        //     $table->bigInteger('user_id');
        //     $table->timestamps();
        // });
        // Schema::create('cms_category_text', function (Blueprint $table) {
        //     $table->bigInteger('category_id');
        //     $table->char('category_lang', 5);
        //     $table->string('category_label');
        //     $table->string('slug');
        // });
        // Schema::create('cms_post', function (Blueprint $table) {
        //     $table->bigIncrements('post_id');
        //     $table->bigInteger('post_category_id');
        //     $table->string('post_keyword');
        //     $table->string('post_img');
        //     $table->bigInteger('user_id');
        //     $table->timestamps();
        // });
        // Schema::create('cms_post_text', function (Blueprint $table) {
        //     $table->bigInteger('post_id');
        //     $table->char('post_lang', 5);
        //     $table->string('post_title');
        //     $table->string('slug');
        //     $table->string('post_post');
        // });
        // Schema::create('cms_menufront', function (Blueprint $table) {
        //     $table->bigIncrements('menu_id');
        //     $table->bigInteger('menu_parent_id');
        //     $table->string('menu_desc');
        //     $table->enum('menu_is_show', ['Y', 'N']);
        //     $table->string('menu_icon_small');
        //     $table->string('menu_icon_large');
        //     $table->bigInteger('menu_order');
        //     $table->bigInteger('user_id');
        //     $table->timestamps();
        // });
        // Schema::create('cms_menufront_text', function (Blueprint $table) {
        //     $table->bigInteger('menu_id');
        //     $table->char('menu_lang', 5);
        //     $table->string('menu_name');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('users');
        // Schema::dropIfExists('cruds');
    }
}

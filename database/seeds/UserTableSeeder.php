<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

	public function run()
	{
	    DB::table('users')->delete();
	    User::create(array(
	        'name' => 'hesti',
	        'email'    => 'hesti@gmail.io',
	        'password' => Hash::make('2017'),
	    ));
	}

}